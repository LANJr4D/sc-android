package net.java.sip.communicator.initializer;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.osgi.framework.*;

/**
 * This activator provides parameters needed by the application before launching
 * its bundles: initializes logging mechanism and loads application properties
 * into system properties.
 * 
 * @author cristina
 * 
 */
public class InitializerActivator
    implements BundleActivator
{
    public void start(BundleContext context)
    {
        loadProperties("application.properties");
        loadLoggingConfig();
    }

    public void stop(BundleContext context)
    {

    }

    /**
     * Reads properties from the given file and sets them as System properties
     * for further use
     * 
     * @param fileName name of the properties file
     */
    private void loadProperties(String fileName)
    {
        InputStream sysPrperties =
            this.getClass().getResourceAsStream(fileName);
        Properties configProps = getConfigProperties(sysPrperties);

        Enumeration keys = configProps.keys();
        while (keys.hasMoreElements())
        {
            String key = keys.nextElement().toString();
            System.setProperty(key, configProps.getProperty(key));
        }
    }

    /**
     * Reads given properties file and loads into a Properties object
     * 
     * @param is properties file
     * @return properties object loaded from properties file
     */
    public Properties getConfigProperties(InputStream is)
    {
        Properties configProperties = new Properties();
        try
        {
            configProperties.load(is);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return configProperties;
    }

    /**
     * Loads logging configuration.
     */
    public void loadLoggingConfig()
    {
        LogManager logManager = LogManager.getLogManager();
        try
        {
            InputStream is =
                this.getClass().getResourceAsStream("logging.properties");
            logManager.readConfiguration(is);

            // build log directory
            String logDirPath =
                logManager.getProperty("java.util.logging.FileHandler.pattern");
            ;
            int index =
                (logDirPath.indexOf("/sip-communicator.log") == -1) ? logDirPath
                    .indexOf("/sip-communicator%u.log")
                    : logDirPath.indexOf("/sip-communicator.log");
            logDirPath = logDirPath.substring(0, index);

            File logDir = new File(logDirPath);
            if (!logDir.exists())
            {
                logDir.mkdir();
            }
        }
        catch (Exception ioe)
        {
            ioe.printStackTrace();
        }

    }
}
