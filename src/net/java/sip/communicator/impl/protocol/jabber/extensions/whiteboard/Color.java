package net.java.sip.communicator.impl.protocol.jabber.extensions.whiteboard;

public class Color
{

    int value;

    public static final Color white;

    public static final Color WHITE;

    public static final Color lightGray;

    public static final Color LIGHT_GRAY;

    public static final Color gray;

    public static final Color GRAY;

    public static final Color darkGray;

    public static final Color DARK_GRAY;

    public static final Color black;

    public static final Color BLACK;

    public static final Color red;

    public static final Color RED;

    public static final Color pink;

    public static final Color PINK;

    public static final Color orange;

    public static final Color ORANGE;

    public static final Color yellow;

    public static final Color YELLOW;

    public static final Color green;

    public static final Color GREEN;

    public static final Color magenta;

    public static final Color MAGENTA;

    public static final Color cyan;

    public static final Color CYAN;

    public static final Color blue;

    public static final Color BLUE;

    static
    {
        white = new Color(255, 255, 255);
        WHITE = white;
        lightGray = new Color(192, 192, 192);
        LIGHT_GRAY = lightGray;
        gray = new Color(128, 128, 128);
        GRAY = gray;
        darkGray = new Color(64, 64, 64);
        DARK_GRAY = darkGray;
        black = new Color(0, 0, 0);
        BLACK = black;
        red = new Color(255, 0, 0);
        RED = red;
        pink = new Color(255, 175, 175);
        PINK = pink;
        orange = new Color(255, 200, 0);
        ORANGE = orange;
        yellow = new Color(255, 255, 0);
        YELLOW = yellow;
        green = new Color(0, 255, 0);
        GREEN = green;
        magenta = new Color(255, 0, 255);
        MAGENTA = magenta;
        cyan = new Color(0, 255, 255);
        CYAN = cyan;
        blue = new Color(0, 0, 255);
        BLUE = blue;
    }

    public Color(int i)
    {
        value = -16777216 | i;
    }

    public Color(int i, int j, int k)
    {
        this(i, j, k, 255);
    }

    public Color(int i, int j, int k, int l)
    {
        value =
            (l & 255) << 24 | (i & 255) << 16 | (j & 255) << 8 | (k & 255) << 0;
        testColorValueRange(i, j, k, l);
    }

    private static void testColorValueRange(int i, int j, int k, int l)
    {
        boolean flag = false;
        String s = "";
        if (l < 0 || l > 255)
        {
            flag = true;
            s = (new StringBuilder()).append(s).append(" Alpha").toString();
        }
        if (i < 0 || i > 255)
        {
            flag = true;
            s = (new StringBuilder()).append(s).append(" Red").toString();
        }
        if (j < 0 || j > 255)
        {
            flag = true;
            s = (new StringBuilder()).append(s).append(" Green").toString();
        }
        if (k < 0 || k > 255)
        {
            flag = true;
            s = (new StringBuilder()).append(s).append(" Blue").toString();
        }
        if (flag)
            throw new IllegalArgumentException((new StringBuilder()).append(
                "Color parameter outside of expected range:").append(s)
                .toString());
        else
            return;
    }

    public int getRGB()
    {
        return value;
    }

    public int getRed()
    {
        return getRGB() >> 16 & 255;
    }

    public int getGreen()
    {
        return getRGB() >> 8 & 255;
    }

    public int getBlue()
    {
        return getRGB() >> 0 & 255;
    }

    public static Color decode(String s) throws NumberFormatException
    {
        Integer integer = Integer.decode(s);
        int i = integer.intValue();
        return new Color(i >> 16 & 255, i >> 8 & 255, i & 255);
    }
    
    public static Color getColor(String s, int i)
    {
        Integer integer = Integer.getInteger(s);
        int j = integer == null ? i : integer.intValue();
        return new Color(j >> 16 & 255, j >> 8 & 255, j >> 0 & 255);
    }
}
