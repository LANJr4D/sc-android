/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.history;

import java.io.*;
import java.util.*;

import net.java.sip.communicator.service.history.records.*;

/**
 * @author Alexander Pelov
 */
public interface HistoryWriter
{

    public static final String TIMESTAMP_ATTRIBUTE = "timestamp";

    /**
     * Stores the passed record complying with the historyRecordStructure.
     * 
     * @param record The record to be added.
     * 
     * @throws IOException
     */
    void addRecord(HistoryRecord record) throws IOException;

    /**
     * Stores the passed propertyValues complying with the
     * historyRecordStructure.
     * 
     * @param propertyValues The values of the record.
     * 
     * @throws IOException
     */
    void addRecord(String[] propertyValues) throws IOException;

    /**
     * Stores the passed propertyValues complying with the
     * historyRecordStructure.
     * 
     * @param propertyValues The values of the record.
     * @param timestamp The timestamp of the record.
     * 
     * @throws IOException
     */
    void addRecord(String[] propertyValues, Date timestamp) throws IOException;

    /**
     * Ads attribute names to the XMLConfigUtils class in order for this one to
     * successfully write the XML configuration file
     * 
     * Android platform does not yet supply a complete implementation for
     * org.w3c.dom.Node thus the method getAttributes() always returns NULL.
     * 
     * Instead of iterating through the attribute list, writeFile() method will
     * iterate through a list of attributes names <b>xmlAttributeNameList</b>
     * and for each not NULL attribute, the value will be written in the XML
     * file
     * 
     */
    public void addAttributeNames();

}
