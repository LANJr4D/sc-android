package net.java.sip.communicator.android;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

import net.java.sip.communicator.android.service.gui.*;

import org.apache.felix.framework.cache.*;
import org.apache.felix.framework.util.*;
import org.osgi.framework.*;

import android.content.*;
import android.util.*;

public class HostActivator
    implements BundleActivator
{
    public static final String AUTO_INSTALL_PROP = "felix.auto.install";

    public static final String AUTO_START_PROP = "felix.auto.start";

    private static final String TAG = "HostActivator";

    private StringMap configMap;

    private BundleContext bundleContext = null;

    private Context androidContext;

    public HostActivator(StringMap configMap, Context androidContext)
    {
        super();
        this.configMap = configMap;
        this.androidContext = androidContext;
    }

    public void start(BundleContext context)
    {
        bundleContext = context;

        // all Android views are initialized using android.content.Context
        // class that the SIPCommunicator activity extends; in order to build
        // views inside android_ui bundle we should provide them such an Android
        // context through AndroidContextTransporterService service; this one
        // will be retrieved by android_ui activator and provided to views that
        // we want to build

        AndroidContextTransporterImpl transporter =
            new AndroidContextTransporterImpl(androidContext);

        // register AndroidContextTransporterService
        bundleContext.registerService(AndroidContextTransporterService.class
            .getName(), transporter, null);
    }

    public void stop(BundleContext context)
    {
        bundleContext = null;
    }

    /**
     * Iterates on felix configuration file, here configMap, in order to
     * install/start enumerated bundles.
     */
    public void installAndStartBundles()
    {

        InputStream bundle;

        Iterator i = configMap.keySet().iterator();

        // start bundles
        i = configMap.keySet().iterator();
        do
        {
            if (!i.hasNext())
                break;
            String key = (String) i.next();
            if (key.startsWith("felix.auto.start"))
            {
                StringTokenizer st =
                    new StringTokenizer((String) configMap.get(key), "\" ",
                        true);
                String location = nextLocation(st);
                while (location != null)
                {
                    // bundles are installed as InputStreams so we need to get
                    // them from the resource folder
                    bundle = getResourceRawStream(location);

                    try
                    {
                        Bundle b =
                            bundleContext.installBundle(location, bundle);

                        Log
                            .i(TAG, "Bundle " + location
                                + " has been installed");

                        if (b != null)
                            b.start();

                        Log.i(TAG, "Bundle " + location + " has been started");

                    }
                    catch (Exception e)
                    {
                        Log.e(TAG, "Auto-properties start: " + e);
                    }
                    location = nextLocation(st);
                }
            }
        }
        while (true);
    }

    /**
     * Returns the next location of a bundle from the felix configuration map.
     * 
     * @param st
     * @return
     */
    private static String nextLocation(StringTokenizer st)
    {
        String retVal = null;
        if (st.countTokens() > 0)
        {
            String tokenList = "\" ";
            StringBuffer tokBuf = new StringBuffer(10);
            String tok = null;
            boolean inQuote = false;
            boolean tokStarted = false;
            boolean exit = false;
            do
            {
                if (!st.hasMoreTokens() || exit)
                    break;
                tok = st.nextToken(tokenList);
                if (tok.equals("\""))
                {
                    inQuote = !inQuote;
                    if (inQuote)
                        tokenList = "\"";
                    else
                        tokenList = "\" ";
                }
                else if (tok.equals(" "))
                {
                    if (tokStarted)
                    {
                        retVal = tokBuf.toString();
                        tokStarted = false;
                        tokBuf = new StringBuffer(10);
                        exit = true;
                    }
                }
                else
                {
                    tokStarted = true;
                    tokBuf.append(tok.trim());
                }
            }
            while (true);
            if (!exit && tokStarted)
                retVal = tokBuf.toString();
        }

        return retVal;
    }

    /**
     * Gets an InputStream from the resource raw directory.
     * 
     * @param resourceName name of the resource
     * @return the corresponding InputStream
     */
    InputStream getResourceRawStream(String resourceName)
    {
        InputStream result = null;

        try
        {
            Class rClass = R.raw.class;
            Field rField = rClass.getDeclaredField(resourceName);
            int fieldValue = rField.getInt(null);

            result = androidContext.getResources().openRawResource(fieldValue);

        }
        catch (NoSuchFieldException nsfe)
        {
            Log.e(TAG, "Error while retrieving InputStream resource", nsfe);
        }
        catch (IllegalAccessException iae)
        {
            Log.e(TAG, "Error while retrieving InputStream resource", iae);
        }

        return result;
    }

    /**
     * The method deletes the directory containing installed bundle cache.
     */
    public boolean cleanBundleCacheDir()
    {
        String bundleCacheDirPath =
            (String) configMap.get(BundleCache.CACHE_PROFILE_DIR_PROP);
        File bundleCacheDir = new File(bundleCacheDirPath);

        return deleteDirectory(bundleCacheDir);
    }

    /**
     * Deletes a non-empty directory and its contents.
     * 
     * @param path File object denoting the non-empty directory
     * @return true if the deleting operation was successful, false otherwise
     */
    static private boolean deleteDirectory(File path)
    {
        boolean result = true;

        if (path.exists())
        {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++)
            {
                if (files[i].isDirectory())
                {
                    deleteDirectory(files[i]);
                }
                else
                {
                    files[i].delete();
                }
            }
            result = path.delete();
        }
        return result;
    }

    class AndroidContextTransporterImpl
        implements AndroidContextTransporterService
    {

        private Context androidContext;

        AndroidContextTransporterImpl(Context androidContext)
        {
            this.androidContext = androidContext;
        }

        public Context getAndroidContext()
        {
            return androidContext;
        }

    }

}
