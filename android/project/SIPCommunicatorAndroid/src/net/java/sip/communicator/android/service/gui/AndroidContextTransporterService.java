package net.java.sip.communicator.android.service.gui;

import android.content.*;

/*
 * All Android views are initialized using android.content.Context class that
 * the SIPCommunicator activity extends; in order to build views inside
 * android_ui bundle we should provide them such an Android context through
 * AndroidContextTransporterService service; this one will be retrieved by
 * android_ui activator and provided to views that we want to build
 */

public interface AndroidContextTransporterService
{
    public Context getAndroidContext();
    
}
