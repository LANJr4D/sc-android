package net.java.sip.communicator.android;

import java.io.*;
import java.util.*;

import net.java.sip.communicator.android.service.gui.UIService;

import org.apache.felix.framework.*;
import org.apache.felix.framework.util.*;
import org.osgi.framework.*;

import android.app.*;
import android.os.*;
import android.util.*;
import android.view.*;

public class SIPCommunicator
    extends Activity
{

    private static final String TAG = "SIPCommunicator";

    private Felix felixInstance = null;

    private static final int MENU_NEW_AACOUNT = 1;

    private static final int MENU_ADD_CONTACT = 2;

    private static final int MENU_ADD_GROUP = 3;

    private static final int MENU_ACCOUNTS = 4;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(android.os.Bundle icicle)
    {
        super.onCreate(icicle);

        Log.i(TAG, "Starting felix launcher thread");
        setContentView(R.layout.splashscreen);

        Thread t = new Thread("SC Felix launcher thread")
        {
            public void run()
            {
                Looper.prepare();

                Handler felixLauncherHandler = new Handler();
                felixLauncherHandler.post(new FelixLauncher());

                Looper.loop();
            }
        };
        t.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        boolean result = super.onCreateOptionsMenu(menu);

        menu.add(0, MENU_NEW_AACOUNT, R.string.menu_new_account);
        menu.add(0, MENU_ACCOUNTS, R.string.menu_accounts);
        menu.add(0, MENU_ADD_CONTACT, R.string.menu_add_contact);
        menu.add(0, MENU_ADD_GROUP, R.string.menu_add_group);

        return result;
    }

    /**
     * Method processing menu item selections.
     */
    @Override
    public boolean onOptionsItemSelected(Menu.Item item)
    {
        switch (item.getId())
        {
        case MENU_NEW_AACOUNT:
        {
            // display newAccountWizard
            // menuItem should be enabled only after mainGUI has been displayed,
            // that's the object of future work
            final UIService uiService = getUIService();

            if (uiService != null)
            {
                runOnUIThread(new Runnable()
                {
                    public void run()
                    {
                        setContentView(uiService
                            .displayNewAccountRegistrationWizard());
                    }
                });
            }

            break;
        }
        case MENU_ACCOUNTS:
        {
            break;
        }
        case MENU_ADD_CONTACT:
        {
            // display addContactWizard
            // menuItem should be enabled only after mainGUI has been displayed,
            // that's the object of future work
            final UIService uiService = getUIService();

            if (uiService != null)
            {
                runOnUIThread(new Runnable()
                {
                    public void run()
                    {
                        setContentView(uiService.displayAddContactWizard());
                    }
                });
            }

            break;
        }
        case MENU_ADD_GROUP:
        {
            break;
        }
        }

        return super.onOptionsItemSelected(item);
    }

    class FelixLauncher
        implements Runnable
    {

        public void run()
        {
            Log.i(TAG, "Launching Felix");

            launchFelix();

            // get UIService reference in order to update GUI with an empty view
            // that will serve as container for the application screens
            final UIService uiService = getUIService();

            // update user interface
            runOnUIThread(new Runnable()
            {
                public void run()
                {
                    SIPCommunicator.this
                        .setContentView(uiService.getRootView());

                    // update UI
                    uiService.loadApplicationGui();
                }
            });

        }

        /**
         * Method launching Felix
         */
        private void launchFelix()
        {
            Log.i(TAG, "Current Thread = " + Thread.currentThread().getName()
                + Thread.currentThread().getId());

            // Read configuration properties.
            Properties configProps = getConfigProperties();

            try
            {
                // Create a case-insensitive property map.
                StringMap configMap = new StringMap(configProps, false);

                // Create host activator;
                HostActivator felixActivator =
                    new HostActivator(configMap, SIPCommunicator.this);
                List list = new ArrayList();
                list.add(felixActivator);

                Log.i(TAG, "Cleaning bundle cache dir");

                // clean bundle cache directory
                felixActivator.cleanBundleCacheDir();

                Log.i(TAG, "Init Felix");

                // Create an instance of the framework.
                felixInstance = new Felix(configMap, list);
                felixInstance.start();

                Log.i(TAG, "Starting bundles");

                // install/start bundles from the configuration file
                felixActivator.installAndStartBundles();

            }
            catch (Exception e)
            {
                Log.e(TAG, "Felix has started", e);
            }
        }

        /**
         * Reads felix configuration file and loads it in a Properties object
         * 
         * @return properties object loaded from felix configuration file
         */
        private Properties getConfigProperties()
        {
            Properties configProperties = new Properties();
            InputStream is =
                (SIPCommunicator.this).getResources().openRawResource(
                    R.raw.config);

            try
            {
                configProperties.load(is);
            }
            catch (IOException e)
            {
                Log.e(TAG, "Error reading configuration file", e);
            }

            return configProperties;
        }

    }

    /**
     * Retrieves UIService from Felix framework.
     * 
     * @return UIService object
     */
    private UIService getUIService()
    {
        BundleContext bundleContext = felixInstance.getBundleContext();

        ServiceReference uiServiceRef =
            bundleContext.getServiceReference(UIService.class.getName());

        final UIService uiService =
            (UIService) bundleContext.getService(uiServiceRef);

        return uiService;
    }
}