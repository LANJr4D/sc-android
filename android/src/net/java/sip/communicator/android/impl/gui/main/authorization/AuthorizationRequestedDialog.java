/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.authorization;

import net.java.sip.communicator.android.impl.gui.*;
import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.view.*;
import android.widget.*;

/**
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AuthorizationRequestedDialog
    extends LinearLayout
    implements View.OnClickListener
{

    public static final int ACCEPT_CODE = 0;

    public static final int REJECT_CODE = 1;

    public static final int IGNORE_CODE = 2;

    public static final int ERROR_CODE = -1;

    private I18NString acceptString = Messages.getI18NString("accept");

    private I18NString rejectString = Messages.getI18NString("reject");

    private I18NString ignoreString = Messages.getI18NString("ignore");

    private Button acceptButton;

    private Button rejectButton;

    private Button ignoreButton;

    private String title =
        Messages.getI18NString("authorizationRequested").getText();

    private Object lock = new Object();

    private int result;

    static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private Logger logger =
        Logger.getLogger(AuthorizationRequestedDialog.class.getName());

    /**
     * Constructs the <tt>RequestAuthorisationDialog</tt>.
     * 
     * @param mainFrame
     * @param contact The <tt>Contact</tt>, which requires authorisation.
     * @param request The <tt>AuthorizationRequest</tt> that will be sent.
     */
    public AuthorizationRequestedDialog(MainFrame mainFrame, Contact contact,
        AuthorizationRequest request)
    {
        super(mainFrame.getAndroidContext());
        this.setOrientation(LinearLayout.VERTICAL);

        Context androidContext = mainFrame.getAndroidContext();

        TextView titleView = new TextView(androidContext);
        titleView.setText(title);

        TextView infoTextView = new TextView(androidContext);
        infoTextView.setText(Messages.getI18NString(
            "authorizationRequestedInfo", new String[]
            { contact.getDisplayName() }).getText());
        infoTextView.setPadding(0, 20, 0, 0);

        LinearLayout buttonLayout = new LinearLayout(androidContext);
        buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
        buttonLayout.setPadding(0, 20, 0, 0);

        acceptButton = new Button(androidContext);
        acceptButton.setText(acceptString.getText());
        acceptButton.setOnClickListener(this);

        rejectButton = new Button(androidContext);
        rejectButton.setText(rejectString.getText());
        rejectButton.setOnClickListener(this);

        ignoreButton = new Button(androidContext);
        ignoreButton.setText(ignoreString.getText());
        ignoreButton.setOnClickListener(this);

        buttonLayout.addView(acceptButton, WRAP_CONTENT);
        buttonLayout.addView(rejectButton, WRAP_CONTENT);
        buttonLayout.addView(ignoreButton, WRAP_CONTENT);

        this.addView(titleView, WRAP_CONTENT);
        this.addView(infoTextView, WRAP_CONTENT);
        this.addView(buttonLayout, WRAP_CONTENT);
    }

    /**
     * Shows this modal dialog.
     * 
     * @return the result code, which shows what was the choice of the user
     */
    public int showDialog()
    {
        UIServiceImpl.setCurrentView(this);

        synchronized (lock)
        {
            try
            {
                lock.wait();
            }
            catch (InterruptedException e)
            {
                logger.error("Error waiting for user response", e);
            }
        }

        return result;
    }

    public void onClick(View arg0)
    {
        String clickedButton = ((Button) arg0).getText().toString();

        if (clickedButton.equals(acceptString.getText()))
        {
            this.result = ACCEPT_CODE;
        }
        else if (clickedButton.equals(rejectString.getText()))
        {
            this.result = REJECT_CODE;
        }
        else if (clickedButton.equals(ignoreString.getText()))
        {
            this.result = IGNORE_CODE;
        }
        else
        {
            this.result = ERROR_CODE;
        }

        synchronized (lock)
        {
            lock.notify();
        }

        UIServiceImpl.displayMainFrame();
    }
}
