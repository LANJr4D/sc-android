/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.*;

import net.java.sip.communicator.android.service.gui.*;
import android.content.*;
import android.widget.*;

/**
 * The <tt>AddContactWizardPage1</tt> is the first page of the "Add Contact"
 * wizard. Contains the <tt>SelectAccountPanel</tt>, where the user should
 * select the account, where the new contact will be created.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AddContactWizardPage1
    extends LinearLayout
    implements WizardPage, CompoundButton.OnCheckedChangeListener
{

    public static final String IDENTIFIER = "SELECT_ACCOUNT_PANEL";

    private SelectAccountPanel selectAccountPanel;

    private WizardContainer wizard;

    static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>AddContactWizardPage1</tt>.
     * 
     * @param newContact An object that collects all user choices through the
     *            wizard.
     * @param protocolProvidersList The list of available
     *            <tt>ProtocolProviderServices</tt>, from which the user
     *            could select.
     */
    public AddContactWizardPage1(WizardContainer wizard, NewContact newContact,
        Iterator protocolProvidersList, Context androidContext)
    {
        super(androidContext);
        this.wizard = wizard;

        selectAccountPanel =
            new SelectAccountPanel(newContact, protocolProvidersList,
                androidContext, this);
        this.addView(selectAccountPanel, WRAP_CONTENT);
    }

    /**
     * Before the panel is displayed checks the selections and enables the next
     * button if a checkbox is already selected or disables it if nothing is
     * selected.
     */
    public void pageShowing()
    {
        setNextButtonAccordingToCheckBox();
    }

    /**
     * Enables the next button when the user makes a choice and disables it if
     * nothing is selected.
     */
    private void setNextButtonAccordingToCheckBox()
    {
        if (selectAccountPanel.isCheckBoxSelected())
            this.wizard.setNextFinishButtonEnabled(true);
        else
            this.wizard.setNextFinishButtonEnabled(false);
    }

    public Object getIdentifier()
    {
        return IDENTIFIER;
    }

    public Object getNextPageIdentifier()
    {
        return AddContactWizardPage2.IDENTIFIER;
    }

    public Object getBackPageIdentifier()
    {
        return IDENTIFIER;
    }

    public Object getWizardForm()
    {
        return selectAccountPanel;
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    public void pageNext()
    {
        selectAccountPanel.setSelectedAccounts();
    }

    public void pageBack()
    {
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        setNextButtonAccordingToCheckBox();
    }
}
