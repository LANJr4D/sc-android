/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.customcontrols.wizard;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.*;
import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.view.*;
import android.widget.*;

/**
 * This class implements a basic wizard dialog, where the programmer can insert
 * one or more Components to act as panels. These panels can be navigated
 * through arbitrarily using the 'Next' or 'Back' buttons, or the dialog itself
 * can be closed using the 'Cancel' button. Note that even though the dialog
 * uses a CardLayout manager, the order of the panels is not linear. Each panel
 * determines at runtime what its next and previous panel will be.
 */
public class Wizard
    implements WizardContainer, PropertyChangeListener
{
    /**
     * The identifier of the summary wizard page.
     */
    String SUMMARY_PAGE_IDENTIFIER = "SUMMARY";

    /**
     * The identifier of the default wizard page.
     */
    String DEFAULT_PAGE_IDENTIFIER = "DEFAULT";

    /**
     * Indicates that the 'Finish' button was pressed to close the dialog.
     */
    public static final int FINISH_RETURN_CODE = 0;

    /**
     * Indicates that the 'Cancel' button was pressed to close the dialog, or
     * the user pressed the close box in the corner of the window.
     */
    public static final int CANCEL_RETURN_CODE = 1;

    /**
     * Indicates that the dialog closed due to an internal error.
     */
    public static final int ERROR_RETURN_CODE = 2;

    /**
     * The String-based action command for the 'Next' button.
     */
    public static final String NEXT_BUTTON_ACTION_COMMAND =
        "NextButtonActionCommand";

    /**
     * The String-based action command for the 'Back' button.
     */
    public static final String BACK_BUTTON_ACTION_COMMAND =
        "BackButtonActionCommand";

    /**
     * The String-based action command for the 'Cancel' button.
     */
    public static final String CANCEL_BUTTON_ACTION_COMMAND =
        "CancelButtonActionCommand";

    /**
     * The i18n text used for the buttons. Loaded from a property resource file.
     */
    static String BACK_TEXT;

    static String NEXT_TEXT;

    static String FINISH_TEXT;

    static String CANCEL_TEXT;

    private WizardModel wizardModel;

    private LinearLayout wizardPageLayout;

    private LinearLayout pageLayout;

    private LinearLayout buttonLayout;

    protected Button backButton;

    protected Button nextButton;

    protected Button cancelButton;

    private static LinearLayout.LayoutParams WRAP_CONTENT_LP =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);

    /**
     * Flag indicating whether the wizard is shown on the screen or not; this
     * lets us know if we can refresh the UI or not
     */
    private boolean wizardIsShown;

    private Vector wizardListeners = new Vector();

    private Logger logger = Logger.getLogger(Wizard.class.getName());

    /**
     * This method accepts a java.awt.Dialog object as the javax.swing.JDialog's
     * parent.
     * 
     * @param owner The java.awt.Dialog object that is the owner of this dialog.
     */
    public Wizard(Context androidContext)
    {
        wizardModel = new WizardModel();
        wizardIsShown = false;
        initComponents(androidContext);
    }

    /**
     * Returns the current model of the wizard dialog.
     * 
     * @return A WizardModel instance, which serves as the model for the wizard
     *         dialog.
     */
    public WizardModel getModel()
    {
        return wizardModel;
    }

    /**
     * Adds the given WizardPage in this wizard. Each WizardPage is identified
     * by a unique Object-based identifier (often a String), which can be used
     * by the setCurrentPanel() method to display the panel at runtime.
     * 
     * @param id An Object-based identifier used to identify the WizardPage
     *            object
     * @param page The WizardPage object to register in this wizard
     */
    public void registerWizardPage(Object id, WizardPage page)
    {
        // Place a reference to it in the model.
        wizardModel.registerPage(id, page);
    }

    /**
     * Removes from the wizard the <tt>WizardPage</tt> corresponding to the
     * given identifier.
     * 
     * @param id The identifer of the wizard page.
     */
    public void unregisterWizardPage(Object id)
    {
        WizardPage wizardPage = wizardModel.getWizardPage(id);
        if (wizardPage != null)
        {

            wizardModel.unregisterPage(id);
        }
    }

    /**
     * Checks whether a page with the given id exists in the wizard.
     * 
     * @param id the identifier of the searched page
     * @return TRUE if the page with the given id exists in the wizard, FALSE
     *         otherwise.
     */
    public boolean containsPage(Object id)
    {
        if (wizardModel.getWizardPage(id) != null)
            return true;
        else
            return false;
    }

    /**
     * Displays the panel identified by the object passed in. This is the same
     * Object-based identified used when registering the panel.
     * 
     * @param id The Object-based identifier of the panel to be displayed.
     */
    public void setCurrentPage(Object id)
    {
        // Get the hashtable reference to the panel that should
        // be displayed. If the identifier passed in is null, then close
        // the dialog.

        if (id == null)
            close(Wizard.ERROR_RETURN_CODE);

        WizardPage oldPanelDescriptor = wizardModel.getCurrentWizardPage();
        if (oldPanelDescriptor != null)
            oldPanelDescriptor.pageHiding();

        wizardModel.setCurrentPanel(id);
        oldPanelDescriptor = wizardModel.getCurrentWizardPage();

        logger.info("RO: setCurrentPage oldPanelDescriptor="
            + oldPanelDescriptor);

        oldPanelDescriptor.pageShowing();

        // Show the panel in the dialog.
        oldPanelDescriptor.pageShown();

        // update Wizard UI with the current page
        wizardPageLayout.removeAllViews();
        if (oldPanelDescriptor instanceof View)
        {
            wizardPageLayout
                .addView((View) oldPanelDescriptor, WRAP_CONTENT_LP);
        }

        if (wizardIsShown)
        {
            refresh();
        }
    }

    /**
     * Method used to listen for property change events from the model and
     * update the dialog's graphical components as necessary.
     * 
     * @param evt PropertyChangeEvent passed from the model to signal that one
     *            of its properties has changed value.
     */
    public void propertyChange(PropertyChangeEvent evt)
    {

        if (evt.getPropertyName().equals(WizardModel.CURRENT_PAGE_PROPERTY))
        {
            resetButtonsToPanelRules();
        }
        else if (evt.getPropertyName().equals(
            WizardModel.NEXT_FINISH_BUTTON_TEXT_PROPERTY))
        {
            nextButton.setText(evt.getNewValue().toString());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.BACK_BUTTON_TEXT_PROPERTY))
        {
            backButton.setText(evt.getNewValue().toString());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.CANCEL_BUTTON_TEXT_PROPERTY))
        {
            cancelButton.setText(evt.getNewValue().toString());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.NEXT_FINISH_BUTTON_ENABLED_PROPERTY))
        {
            logger.info("RO NEXT_FINISH_BUTTON_ENABLED_PROPERTY");
            nextButton.setEnabled(((Boolean) evt.getNewValue()).booleanValue());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.BACK_BUTTON_ENABLED_PROPERTY))
        {
            backButton.setEnabled(((Boolean) evt.getNewValue()).booleanValue());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.CANCEL_BUTTON_ENABLED_PROPERTY))
        {
            cancelButton.setEnabled(((Boolean) evt.getNewValue())
                .booleanValue());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.NEXT_FINISH_BUTTON_ICON_PROPERTY))
        {
            // TODO nextButton.setIcon((Bitmap) evt.getNewValue());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.BACK_BUTTON_ICON_PROPERTY))
        {
            // TODO backButton.setIcon((Bitmap) evt.getNewValue());
        }
        else if (evt.getPropertyName().equals(
            WizardModel.CANCEL_BUTTON_ICON_PROPERTY))
        {
            // TODO cancelButton.setIcon((Bitmap) evt.getNewValue());
        }

    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @return A boolean indicating if the button is enabled.
     */
    public boolean isBackButtonEnabled()
    {
        return wizardModel.getBackButtonEnabled().booleanValue();
    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @param newValue The new enabled status of the button.
     */
    public void setBackButtonEnabled(boolean newValue)
    {
        wizardModel.setBackButtonEnabled(new Boolean(newValue));
    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @return A boolean indicating if the button is enabled.
     */
    public boolean isNextFinishButtonEnabled()
    {
        return wizardModel.getNextFinishButtonEnabled().booleanValue();
    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @param newValue The new enabled status of the button.
     */
    public void setNextFinishButtonEnabled(boolean newValue)
    {
        logger.info("RO: setNextFinishButtonEnabled");

        wizardModel.setNextFinishButtonEnabled(new Boolean(newValue));
    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @return A boolean indicating if the button is enabled.
     */
    public boolean isCancelButtonEnabled()
    {
        return wizardModel.getCancelButtonEnabled().booleanValue();
    }

    /**
     * Mirrors the WizardModel method of the same name.
     * 
     * @param newValue The new enabled status of the button.
     */
    public void setCancelButtonEnabled(boolean newValue)
    {
        wizardModel.setCancelButtonEnabled(new Boolean(newValue));
    }

    /**
     * Closes the dialog and sets the return code to the integer parameter.
     * 
     * @param code The return code.
     */
    void close(int code)
    {
        WizardPage oldPanelDescriptor = wizardModel.getCurrentWizardPage();
        if (oldPanelDescriptor != null)
            oldPanelDescriptor.pageHiding();

        if (code == CANCEL_RETURN_CODE)
            this.fireWizardEvent(WizardEvent.CANCEL);
        else if (code == FINISH_RETURN_CODE)
            this.fireWizardEvent(WizardEvent.SUCCESS);
        else if (code == ERROR_RETURN_CODE)
            this.fireWizardEvent(WizardEvent.ERROR);

        wizardIsShown = false;

        // TODO close dialog

    }

    /**
     * This method initializes the components for the wizard dialog: builds a
     * container for the wizard pages as well as three buttons at the bottom.
     */

    private void initComponents(Context androidContext)
    {

        wizardModel.addPropertyChangeListener(this);
        /*
         * Create the outer button panel, which is responsible for three
         * buttons: Next, Back, and Cancel. It is also responsible a View above
         * them that uses a LinearLayout layout manager.
         */

        buttonLayout = new LinearLayout(androidContext);
        buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams buttonLp =
            new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.FILL_PARENT);

        backButton = new Button(androidContext);
        buttonLayout.addView(backButton, 0, buttonLp);

        nextButton = new Button(androidContext);
        buttonLayout.addView(nextButton, 1, buttonLp);

        cancelButton = new Button(androidContext);
        buttonLayout.addView(cancelButton, 2, buttonLp);

        buttonLayout.setGravity(Gravity.BOTTOM);

        wizardPageLayout = new LinearLayout(androidContext);
        wizardPageLayout.setOrientation(LinearLayout.VERTICAL);

        pageLayout = new LinearLayout(androidContext);
        pageLayout.setOrientation(LinearLayout.VERTICAL);

        pageLayout.addView(wizardPageLayout, new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.FILL_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT));
        pageLayout.addView(buttonLayout, WRAP_CONTENT_LP);

        backButton.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View view)
            {
                WizardPage page = wizardModel.getCurrentWizardPage();

                page.pageBack();

                Object backPageIdentifier = page.getBackPageIdentifier();

                setCurrentPage(backPageIdentifier);

            }

        }

        );
        nextButton.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View view)
            {

                logger.info("RO: nextButton onClick ");

                logger.info("RO nextButton onClick currentThread="
                    + Thread.currentThread().getName());

                WizardPage page = wizardModel.getCurrentWizardPage();

                try
                {
                    page.pageNext();
                }
                catch (Exception ex)
                {
                    // lots of things may fail on page next, like for example
                    // parameter
                    // validation or account initialization. If this is what
                    // happened here
                    // just show an error and leave everything on the same page
                    // so that
                    // the user would have the chance to correct errors.

                    // TODO display error view - eventual un TOAST/ Dialog
                    return;
                }

                Object nextPageIdentifier = page.getNextPageIdentifier();

                logger.info("RO: nextButton onClick nextPageIdentifier="
                    + nextPageIdentifier);

                if (nextPageIdentifier
                    .equals(WizardPage.FINISH_PAGE_IDENTIFIER))
                {
                    try
                    {
                        wizardIsShown = false;
                        close(Wizard.FINISH_RETURN_CODE);
                        UIServiceImpl.displayMainFrame();
                    }
                    catch (Exception ex)
                    {
                        logger.error("RO: displayMainFrame ", ex);
                        return;
                    }
                }
                else
                {
                    setCurrentPage(nextPageIdentifier);
                }

            }

        }

        );
        cancelButton.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View view)
            {
                wizardIsShown = false;
                UIServiceImpl.displayMainFrame();
            }

        }

        );

    }

    static
    {
        BACK_TEXT = Messages.getI18NString("previous").getText();
        NEXT_TEXT = Messages.getI18NString("next").getText();
        CANCEL_TEXT = Messages.getI18NString("cancel").getText();
        FINISH_TEXT = Messages.getI18NString("finish").getText();
    }

    public void addWizardListener(WizardListener l)
    {
        synchronized (wizardListeners)
        {
            if (!wizardListeners.contains(l))
                wizardListeners.add(l);
        }
    }

    public void removeWizardListener(WizardListener l)
    {
        synchronized (wizardListeners)
        {
            if (wizardListeners.contains(l))
                wizardListeners.remove(l);
        }
    }

    private void fireWizardEvent(int eventCode)
    {
        WizardEvent wizardEvent = new WizardEvent(this, eventCode);

        Iterator listeners = null;
        synchronized (wizardListeners)
        {
            listeners = new ArrayList(wizardListeners).iterator();
        }

        while (listeners.hasNext())
        {
            WizardListener l = (WizardListener) listeners.next();
            l.wizardFinished(wizardEvent);
        }
    }

    public Button getNextButton()
    {
        return this.nextButton;
    }

    public Button getBackButton()
    {
        return this.backButton;
    }

    /**
     * Resets the buttons to support the original panel rules, including whether
     * the next or back buttons are enabled or disabled, or if the panel is
     * finishable. If the panel in question has another panel behind it, enables
     * the back button. Otherwise, disables it. If the panel in question has one
     * or more panels in front of it, enables the next button. Otherwise,
     * disables it.
     */
    void resetButtonsToPanelRules()
    {

        WizardModel model = wizardModel;
        WizardPage page = model.getCurrentWizardPage();

        model.setCancelButtonText(Wizard.CANCEL_TEXT);

        model.setBackButtonText(Wizard.BACK_TEXT);

        if (page.getBackPageIdentifier() != null)
        {
            model.setBackButtonEnabled(Boolean.TRUE);
        }
        else
        {
            model.setBackButtonEnabled(Boolean.FALSE);
        }

        if (page.getNextPageIdentifier() != null)
        {
            model.setNextFinishButtonEnabled(Boolean.TRUE);

        }
        else
        {
            model.setNextFinishButtonEnabled(Boolean.FALSE);
        }

        if (page.getNextPageIdentifier().equals(
            WizardPage.FINISH_PAGE_IDENTIFIER))
        {

            model.setNextFinishButtonText(Wizard.FINISH_TEXT);
        }
        else
        {
            model.setNextFinishButtonText(Wizard.NEXT_TEXT);
        }

    }

    /**
     * Method forcing container views to update their appearence.This method is
     * generally called after modifying content/layout of certain GUI components
     * belonging to the wizard.
     */
    public void refresh()
    {
        UIThreadUtilities.runOnUIThread(pageLayout, new Runnable()
        {
            public void run()
            {
                logger.info("RO setCurrentPage refreshingView");

                wizardPageLayout.invalidate();
                buttonLayout.invalidate();
            }
        });
    }

    public LinearLayout getWizardView()
    {
        return pageLayout;
    }

    public void setWizardIsShown(boolean wizardIsShown)
    {
        this.wizardIsShown = wizardIsShown;
    }
}
