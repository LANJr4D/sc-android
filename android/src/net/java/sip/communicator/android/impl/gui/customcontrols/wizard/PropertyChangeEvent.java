package net.java.sip.communicator.android.impl.gui.customcontrols.wizard;

import java.util.EventObject;

public class PropertyChangeEvent
    extends EventObject
{

    public PropertyChangeEvent(Object obj, String s, Object obj1, Object obj2)
    {
        super(obj);
        propertyName = s;
        newValue = obj2;
        oldValue = obj1;
    }

    public String getPropertyName()
    {
        return propertyName;
    }

    public Object getNewValue()
    {
        return newValue;
    }

    public Object getOldValue()
    {
        return oldValue;
    }

    public void setPropagationId(Object obj)
    {
        propagationId = obj;
    }

    public Object getPropagationId()
    {
        return propagationId;
    }

    private String propertyName;

    private Object newValue;

    private Object oldValue;

    private Object propagationId;
}

/*
 * DECOMPILATION REPORT
 * 
 * Decompiled from: C:\Program Files\Java\jre1.6.0_03\lib\rt.jar Total time: 30
 * ms Jad reported messages/errors: Exit status: 0 Caught exceptions:
 */