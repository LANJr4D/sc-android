/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.service.contactlist.*;
import android.content.*;
import android.graphics.*;
import android.view.View;
import android.widget.*;

/**
 * The <tt>SelectGroupPanel</tt> is where the user should select the group, in
 * which the new contact will be added.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class SelectGroupPanel
    extends LinearLayout
    implements AdapterView.OnItemClickListener
{
    private Spinner groupCombo;

    private AddContactWizard parentWizard;

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>SelectGroupPanel</tt>.
     * 
     * @param wizard the wizard where this panel is contained
     * @param newContact An object that collects all user choices through the
     *            wizard.
     * @param groupsList The list of all <tt>MetaContactGroup</tt>s, from
     *            which the user could select.
     */
    public SelectGroupPanel(AddContactWizard wizard, NewContact newContact,
        Context androidContext)
    {
        super(androidContext);
        this.setOrientation(LinearLayout.VERTICAL);

        this.parentWizard = wizard;

        this.groupCombo = new Spinner(androidContext);

        TextView selectGroupTitle = new TextView(androidContext);
        selectGroupTitle.setText(Messages.getI18NString(
            "selectGroupWizardTitle").getText());

        TextView selectGroupInfo = new TextView(androidContext);
        selectGroupInfo.setText(Messages.getI18NString("selectGroup").getText()
            + ": ");

        Iterator groupsList = wizard.getMainFrame().getAllGroups();

        if (groupsList.hasNext())
        {
            selectGroupInfo.setText(Messages.getI18NString("selectGroupWizard")
                .getText());

            groupCombo.setAdapter(new GroupComboAdapter(groupsList,
                androidContext));
            groupCombo.setSelection(0);
        }
        else
        {
            selectGroupInfo.setTextColor(Color.RED);
            selectGroupInfo.setText(Messages.getI18NString(
                "createFirstGroupWizard").getText());
        }

        this.addView(selectGroupTitle, WRAP_CONTENT);
        this.addView(selectGroupInfo, WRAP_CONTENT);
        this.addView(groupCombo, WRAP_CONTENT);
    }

    /**
     * Returns the selected group.
     * 
     * @return the selected group
     */
    public MetaContactGroup getSelectedGroup()
    {
        Object selectedGroup = groupCombo.getSelectedItem();

        if (selectedGroup != null)
            return ((MetaContactGroup) selectedGroup);

        return null;
    }

    /**
     * 
     */
    public void setNextButtonAccordingToComboBox()
    {
        if (groupCombo.getSelectedItem() != null)
        {
            parentWizard.setNextFinishButtonEnabled(true);
        }
        else
        {
            parentWizard.setNextFinishButtonEnabled(false);
        }
    }

    private class GroupWrapper
    {
        private String groupName;

        private MetaContactGroup group;

        public GroupWrapper(MetaContactGroup group)
        {
            this.group = group;
            this.groupName = group.getGroupName();
        }

        public GroupWrapper(String groupName, MetaContactGroup group)
        {
            this.group = group;
            this.groupName = groupName;
        }

        public String toString()
        {
            return groupName;
        }

        public MetaContactGroup getMetaGroup()
        {
            return this.group;
        }
    }

    public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3)
    {
        this.setNextButtonAccordingToComboBox();
    }

}
