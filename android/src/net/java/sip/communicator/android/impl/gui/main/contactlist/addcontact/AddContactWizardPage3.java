/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import net.java.sip.communicator.android.impl.gui.customcontrols.wizard.*;
import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.android.service.gui.*;
import android.content.*;
import android.graphics.*;
import android.widget.*;

/**
 * The <tt>AddContactWizardPage3</tt> is the last page of the "Add Contact"
 * wizard. Contains the input where the user should enter the identifier of the
 * contact to add.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AddContactWizardPage3
    extends LinearLayout
    implements WizardPage
{

    public static final String IDENTIFIER = "ADD_CONTACT_PANEL";

    private NewContact newContact;

    private EditText uinEdit;

    private TextView emptyEditErrorMessage;

    private Wizard wizard;

    private Object nextPageIdentifier = WizardPage.FINISH_PAGE_IDENTIFIER;

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>AddContactWizardPage3</tt>.
     * 
     * @param newContact An object that collects all user choices through the
     *            wizard.
     */
    public AddContactWizardPage3(Wizard wizard, NewContact newContact,
        Context androidContext)
    {
        super(androidContext);
        this.setOrientation(LinearLayout.VERTICAL);

        this.wizard = wizard;

        TextView titleView = new TextView(androidContext);
        titleView.setText(Messages.getI18NString("addContact").getText());

        LinearLayout uinEditLayout = new LinearLayout(androidContext);
        uinEditLayout.setOrientation(LinearLayout.VERTICAL);

        TextView uin = new TextView(androidContext);
        uin.setText(Messages.getI18NString("addContactIdentifier").getText());

        uinEdit = new EditText(androidContext);
        uinEdit.setTextSize(14f);

        uinEditLayout.addView(uin, WRAP_CONTENT);
        uinEditLayout.addView(uinEdit, WRAP_CONTENT);

        emptyEditErrorMessage = new TextView(androidContext);
        emptyEditErrorMessage.setText("* User ID field should be filled up.");
        emptyEditErrorMessage.setTextColor(Color.RED);

        this.newContact = newContact;

        this.addView(titleView, WRAP_CONTENT);
        this.addView(uinEditLayout, WRAP_CONTENT);
    }

    /**
     * Implements the <tt>WizardPanelDescriptor</tt> method to return the
     * identifier of the next wizard page.
     */
    public Object getNextPageIdentifier()
    {
        return nextPageIdentifier;
    }

    /**
     * Implements the <tt>WizardPanelDescriptor</tt> method to return the
     * identifier of the previous wizard page.
     */
    public Object getBackPageIdentifier()
    {
        return AddContactWizardPage2.IDENTIFIER;
    }

    /**
     * Before finishing the wizard sets the identifier entered by the user to
     * the <tt>NewContact</tt> object.
     */
    public void pageHiding()
    {
        newContact.setUin(uinEdit.getText().toString());
    }

    public Object getIdentifier()
    {
        return IDENTIFIER;
    }

    public Object getWizardForm()
    {
        return uinEdit;
    }

    public void pageShown()
    {

    }

    public void pageShowing()
    {
        this.setNextFinishButtonAccordingToUIN();
    }

    public void pageNext()
    {
        String data = uinEdit.getText().toString();

        if (wizard != null)
        {
            if (data != null && data.trim().length() > 0)
            {
                nextPageIdentifier = WizardPage.FINISH_PAGE_IDENTIFIER;
            }
            else
            {
                if (this.indexOfChild(emptyEditErrorMessage) == -1)
                {
                    this.addView(emptyEditErrorMessage, 0, WRAP_CONTENT);
                }

                nextPageIdentifier = IDENTIFIER;
            }
        }
    }

    public void pageBack()
    {

    }

    public void setNextFinishButtonAccordingToUIN()
    {
        wizard.setNextFinishButtonEnabled(true);
    }
}
