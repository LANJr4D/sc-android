package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import net.java.sip.communicator.service.protocol.*;
import android.content.*;
import android.widget.*;

/**
 * Class extending a regular checkBox in order to provide access to corresponding
 * ProtocolProvider in the AdVontactWizzard
 * 
 * @author cristina
 * 
 */
public class AccountCheckBox
    extends CheckBox
{

    private ProtocolProviderService protocolProviderService;

    public AccountCheckBox(ProtocolProviderService pps, Context androidContext)
    {
        super(androidContext);
        this.protocolProviderService = pps;
    }

    public ProtocolProviderService getProtocolProviderService()
    {
        return protocolProviderService;
    }

    public void setProtocolProviderService(ProtocolProviderService pps)
    {
        this.protocolProviderService = pps;
    }

}
