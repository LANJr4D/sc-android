/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.Iterator;

import net.java.sip.communicator.android.service.gui.*;
import android.content.*;
import android.widget.*;

/**
 * The <tt>AddContactWizardPage2</tt> is the second page of the "Add Contact"
 * wizard. Contains the <tt>SelectGroupPanel</tt>, where the user should
 * select the group, where the new contact will be added.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AddContactWizardPage2
    extends LinearLayout
    implements WizardPage
{
    public static final String IDENTIFIER = "SELECT_GROUP_PANEL";

    private SelectGroupPanel selectGroupPanel;

    private WizardContainer wizard;

    private NewContact newContact;

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>AddContactWizardPage2</tt>.
     * 
     * @param wizard the parent wizard, where this page is contained
     * @param newContact An object that collects all user choices through the
     *            wizard.
     * @param groupsList The list of all <tt>MetaContactGroup</tt>s, from
     *            which the user could select.
     */
    public AddContactWizardPage2(AddContactWizard wizard,
        NewContact newContact, Iterator groupsList, Context androidContext)
    {
        super(androidContext);
        this.wizard = wizard;

        this.newContact = newContact;

        selectGroupPanel =
            new SelectGroupPanel(wizard, newContact, androidContext);
        this.addView(selectGroupPanel, WRAP_CONTENT);
    }

    /**
     * Implements the <tt>WizardPanelDescriptor</tt> method to return the
     * identifier of the next wizard page.
     */
    public Object getNextPageIdentifier()
    {
        return AddContactWizardPage3.IDENTIFIER;
    }

    /**
     * Implements the <tt>WizardPanelDescriptor</tt> method to return the
     * identifier of the previous wizard page.
     */
    public Object getBackPageIdentifier()
    {
        return AddContactWizardPage1.IDENTIFIER;
    }

    /**
     * Before the panel is displayed checks the selections and enables the next
     * button if a checkbox is already selected or disables it if nothing is
     * selected.
     */
    public void pageShowing()
    {
        selectGroupPanel.setNextButtonAccordingToComboBox();
    }

    public Object getIdentifier()
    {
        return IDENTIFIER;
    }

    public Object getWizardForm()
    {
        return selectGroupPanel;
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    public void pageNext()
    {
        this.newContact.setGroup(selectGroupPanel.getSelectedGroup());
    }

    public void pageBack()
    {
    }
}
