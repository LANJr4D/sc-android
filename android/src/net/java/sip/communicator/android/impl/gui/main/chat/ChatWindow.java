package net.java.sip.communicator.android.impl.gui.main.chat;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.android.impl.gui.utils.*;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.graphics.*;
import android.text.*;
import android.text.style.*;
import android.view.*;
import android.widget.*;

public class ChatWindow
    extends LinearLayout
{

    private Logger logger = Logger.getLogger(ChatWindow.class.getName());

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private MetaContact metaContact;

    private EditText msgEdit;

    /**
     * View where sent/received messages are displayed
     */
    private EditText msgHistoryView;

    private boolean isShown;

    public ChatWindow(Context androidContext, MetaContact metaContact)
    {
        super(androidContext);
        this.metaContact = metaContact;

        Context parentContext = this.mContext;

        this.setOrientation(LinearLayout.VERTICAL);

        ScrollView msgHistoryLayout = new ScrollView(parentContext);

        msgHistoryView = new EditText(parentContext);
        msgHistoryView.setLines(14);
        msgHistoryView.setTextSize(13f);
        msgHistoryView.setMaxWidth(300);
        msgHistoryView.setMinWidth(300);
        msgHistoryView.setBackgroundColor(Color.WHITE);
        msgHistoryView.setTextColor(Color.DKGRAY);
        msgHistoryView.setPreferredHeight(400);
        msgHistoryView.setText("Log ...");

        msgHistoryLayout.addView(msgHistoryView, WRAP_CONTENT);

        // message editing view
        LinearLayout msgLayout = new LinearLayout(parentContext);
        msgLayout.setOrientation(LinearLayout.HORIZONTAL);

        ScrollView msgEditLayout = new ScrollView(parentContext);

        msgEdit = new EditText(parentContext);
        msgEdit.setMaxWidth(250);
        msgEdit.setMinWidth(250);
        msgEdit.setLines(3);

        msgEditLayout.addView(msgEdit, WRAP_CONTENT);

        Button sendButton = new Button(parentContext);
        sendButton.setText("Send");
        sendButton.setOnClickListener(new OnClickListener()
        {

            public void onClick(View view)
            {
                final String messageText = msgEdit.getText().toString();

                if ((messageText != null) && (!messageText.trim().equals("")))
                {

                    new Thread()
                    {
                        public void run()
                        {
                            sendInstantMessage(messageText);
                        }
                    }.start();
                }
                // make sure the focus goes back to the write area
                // TODO chatPanel.requestFocusInWriteArea();
            }

        });

        msgLayout.addView(msgEditLayout, WRAP_CONTENT);
        msgLayout.addView(sendButton, WRAP_CONTENT);

        this.addView(msgHistoryLayout, WRAP_CONTENT);
        this.addView(msgLayout, WRAP_CONTENT);
    }

    public MetaContact getContact()
    {
        return metaContact;
    }

    public void setContact(MetaContact contact)
    {
        this.metaContact = contact;
    }

    private void sendInstantMessage(String text)
    {
        Contact contact = (Contact) metaContact.getDefaultContact();

        OperationSetBasicInstantMessaging im =
            (OperationSetBasicInstantMessaging) contact.getProtocolProvider()
                .getOperationSet(OperationSetBasicInstantMessaging.class);

        Message msg = im.createMessage(text);

        try
        {
            im.sendInstantMessage(contact, msg);

            this.processMessage("me", new Date(System.currentTimeMillis()),
                Constants.OUTGOING_MESSAGE, msg.getContent(), msg
                    .getContentType());
        }
        catch (IllegalStateException ex)
        {
            logger.error("Failed to send message.", ex);

            this.processMessage("me", new Date(System.currentTimeMillis()),
                Constants.OUTGOING_MESSAGE, msg.getContent(), msg
                    .getContentType());

            this.processMessage("me", new Date(System.currentTimeMillis()),
                Constants.ERROR_MESSAGE, Messages.getI18NString(
                    "msgSendConnectionProblem").getText(), "text");
        }
        catch (Exception ex)
        {
            logger.error("Failed to send message.", ex);

            this.processMessage("me", new Date(System.currentTimeMillis()),
                Constants.OUTGOING_MESSAGE, msg.getContent(), msg
                    .getContentType());

            this.processMessage("me", new Date(System.currentTimeMillis()),
                Constants.ERROR_MESSAGE, Messages.getI18NString(
                    "msgDeliveryInternalError").getText(), "text");
        }
    }

    /**
     * Passes the message to the contained <code>ChatConversationPanel</code>
     * for processing and appends it at the end of the conversationPanel
     * document.
     * 
     * @param contactName The name of the contact sending the message.
     * @param date The time at which the message is sent or received.
     * @param messageType The type of the message. One of OUTGOING_MESSAGE or
     *            INCOMING_MESSAGE.
     * @param message The message text.
     */
    public void processMessage(final String contactName, final Date date,
        final String messageType, final String message, String contentType)
    {
        UIThreadUtilities.runOnUIThread(this, new Runnable()
        {

            public void run()
            {
                if (messageType.equals(Constants.OUTGOING_MESSAGE))
                {
                    // refresh write area
                    msgEdit.setText("");
                }

                // process sent message
                StringBuffer contactInfo = new StringBuffer("\n");
                contactInfo.append(contactName).append(" at ").append(
                    GuiUtils.formatTime(date)).append("\n");

                msgHistoryView.append(contactInfo.toString());

                // set color on contact message information
                // I know there might be contacts with this ID - "me", but it's
                // just a test so I'll keep it simple ;)
                int contactColor =
                    (contactName.equals("me")) ? Color.rgb(51, 153, 255)
                        : Color.rgb(255, 153, 51);

                int contactInfoSize = contactInfo.length();
                Spannable span = msgHistoryView.getText();
                span.setSpan(new BackgroundColorSpan(contactColor), span
                    .length()
                    - contactInfoSize, span.length() - 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                // append sent message to the msgHistoryView
                msgHistoryView.append(message);

                msgEdit.invalidate();
                msgHistoryView.invalidate();
            }
        });
    }

    public boolean isShown()
    {
        return isShown;
    }

    public void setShown(boolean isShown)
    {
        this.isShown = isShown;
    }

    public MetaContact getMetaContact()
    {
        return metaContact;
    }

}
