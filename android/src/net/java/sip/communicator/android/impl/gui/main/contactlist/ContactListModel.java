/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main.contactlist;

import java.util.*;

import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.view.*;
import android.widget.*;

/**
 * The list model of the ContactList. This class use as a data model the
 * <tt>MetaContactListService</tt> itself. It provides an implementation of
 * the BaseAdapter and adds some methods facilitating the access to the contact
 * list. Some more contact list specific methods are added like:
 * getMetaContactStatus, getMetaContactStatusIcon, changeContactStatus, etc.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 * 
 */
public class ContactListModel
    extends BaseAdapter
{
    private static final Logger logger =
        Logger.getLogger(ContactListModel.class);

    private MetaContactListService contactList;

    private MetaContactGroup rootGroup;

    private Context androidContext;

    /**
     * A list of all contacts that are currently "active". An "active" contact
     * is a contact that has been sent a message. The list is used to indicate
     * these contacts with a special icon.
     */
    private Vector activeContacts = new Vector();

    private boolean showOffline = true;

    /**
     * Creates a List Model, which gets its data from the given
     * MetaContactListService.
     * 
     * @param contactList The MetaContactListService which contains the contact
     *            list.
     */
    public ContactListModel(MetaContactListService contactList,
        Context androidContext)
    {
        this.contactList = contactList;
        this.androidContext = androidContext;
        this.rootGroup = this.contactList.getRoot();
    }

    /**
     * Returns the object at the given index.
     * 
     * @param index The index.
     * @return The object at the given index.
     */
    public Object getElementAt(int index)
    {
        Object element = this.getElementAt(this.rootGroup, -1, index);

        return element;
    }

    /**
     * Goes through all subgroups and contacts and determines the final size of
     * the contact list.
     * 
     * @param group The group which to be measured.
     * @return The size of the contactlist
     */
    private int getContactListSize(MetaContactGroup group)
    {
        int size = 0;

        // if (!isGroupClosed(group))
        // {
        // if (showOffline)
        // {
        // size = group.countChildContacts();
        // // count the group itself
        // if (!group.equals(rootGroup))
        // size++;
        // }
        // else
        // {
        // Iterator i = group.getChildContacts();
        // while (i.hasNext())
        // {
        // MetaContact contact = (MetaContact) i.next();
        //
        // if (isContactOnline(contact))
        // size++;
        // }
        //
        // // count the group itself only if it contains any online
        // // contacts
        // if (!group.equals(rootGroup) && size > 0)
        // size++;
        // }
        //
        // Iterator subgroups = group.getSubgroups();
        //
        // while (subgroups.hasNext())
        // {
        // MetaContactGroup subGroup = (MetaContactGroup) subgroups.next();
        // size += getContactListSize(subGroup);
        // }
        // }
        // else
        // {
        // // If offline contacts are shown we just count the closed group;
        // if (showOffline)
        // {
        // // count the closed group
        // size++;
        // }
        // else
        // {
        // // If offline contacts are not shown we'll count the group
        // // only if it contains online contacts.
        // if (containsOnlineContacts(group))
        // size++;
        // }
        // }

        return size;
    }

    /**
     * Returns the general status of the given MetaContact. Detects the status
     * using the priority status table. The priority is defined on the
     * "availablity" factor and here the most "available" status is returned.
     * 
     * @param metaContact The metaContact fot which the status is asked.
     * @return PresenceStatus The most "available" status from all subcontact
     *         statuses.
     */
    public PresenceStatus getMetaContactStatus(MetaContact metaContact)
    {
        PresenceStatus status = null;
        Iterator i = metaContact.getContacts();
        while (i.hasNext())
        {
            Contact protoContact = (Contact) i.next();
            PresenceStatus contactStatus = protoContact.getPresenceStatus();

            if (status == null)
            {
                status = contactStatus;
            }
            else
            {
                status =
                    (contactStatus.compareTo(status) > 0) ? contactStatus
                        : status;
            }
        }
        return status;
    }

    /**
     * If the given object is instance of MetaContact or MetaContactGroup
     * returns the index of this meta contact or group, otherwiser returns -1.
     * 
     * @param o the object, which index we search
     * @return the index of the given object if it founds it, otherwise -1
     */
    public int indexOf(Object o)
    {
        if (o instanceof MetaContact)
        {
            return this.indexOf((MetaContact) o);
        }
        else if (o instanceof MetaContactGroup)
        {
            return this.indexOf((MetaContactGroup) o);
        }
        else
        {
            return -1;
        }
    }

    /**
     * Returns the index of the given MetaContact.
     * 
     * @param contact The MetaContact to search for.
     * @return The index of the given MetaContact.
     */
    private int indexOf(MetaContact contact)
    {
        int index = -1;

        rootGroup.indexOf(contact);

        if (showOffline || isContactOnline(contact))
        {
            int currentIndex = 0;
            MetaContactGroup parentGroup =
                this.contactList.findParentMetaContactGroup(contact);

            if (parentGroup != null)
            {

                currentIndex += this.indexOf(parentGroup);

                currentIndex += parentGroup.indexOf(contact) + 1;

                index = currentIndex;
            }
        }
        return index;
    }

    /**
     * Recursively searches the given group in depth for the element at the
     * given index.
     * 
     * @param group the group in which we search
     * @param currentIndex the index, where we currently are
     * @param searchedIndex the index to search for
     * @return The element at the given index, if we find it, otherwise null.
     */
    private Object getElementAt(MetaContactGroup group, int currentIndex,
        int searchedIndex)
    {

        Object element = null;
        // if (currentIndex == searchedIndex)
        // {
        // // the current index is the index of the group so if this is the
        // // searched index we return the group
        // element = group;
        // }
        // else
        // {
        // // if the group is closed don't count its children
        // if (!isGroupClosed(group))
        // {
        // int childCount = countChildContacts(group);
        //
        // if (searchedIndex <= (currentIndex + childCount))
        // {
        // // if the searched index is lower than or equal to
        // // the greater child index in this group then our element is
        // // here
        // MetaContact contact =
        // group.getMetaContact(searchedIndex - currentIndex - 1);
        //
        // if (showOffline || isContactOnline(contact))
        // element = contact;
        // }
        // else
        // {
        // // if we haven't found the contact we search the subgroups
        // currentIndex += childCount;
        // Iterator subgroups = group.getSubgroups();
        //
        // while (subgroups.hasNext())
        // {
        // MetaContactGroup subgroup =
        // (MetaContactGroup) subgroups.next();
        //
        // if (showOffline || containsOnlineContacts(subgroup))
        // element =
        // getElementAt(subgroup, currentIndex + 1,
        // searchedIndex);
        //
        // if (element != null)
        // break;
        // else
        // {
        // // if we haven't found the element on this iteration
        // // we update the current index and we continue
        // if (showOffline || containsOnlineContacts(subgroup))
        // {
        // if (!isGroupClosed(subgroup))
        // currentIndex +=
        // countChildContacts(subgroup) + 1;
        // else
        // currentIndex++;
        // }
        // }
        // }
        // }
        // }
        // }
        return element;
    }

    /**
     * Returns true if offline contacts should be shown, false otherwise.
     * 
     * @return boolean true if offline contacts should be shown, false
     *         otherwise.
     */
    public boolean isShowOffline()
    {
        return showOffline;
    }

    /**
     * Sets the showOffline variable to indicate whether or not offline contacts
     * should be shown.
     * 
     * @param showOffline true if offline contacts should be shown, false
     *            otherwise.
     */
    public void setShowOffline(boolean showOffline)
    {
        this.showOffline = showOffline;
    }

    /**
     * Returns TRUE if the given meta contact is online, FALSE otherwise.
     * 
     * @param contact the meta contact
     * @return TRUE if the given meta contact is online, FALSE otherwise
     */
    public boolean isContactOnline(MetaContact contact)
    {
        // If for some reason the default contact is null we return false.
        if (contact.getDefaultContact() == null)
            return false;

        // Lays on the fact that the default contact is the most connected.
        if (contact.getDefaultContact().getPresenceStatus().getStatus() >= PresenceStatus.ONLINE_THRESHOLD)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Counts group child contacts depending on the showOffline option.
     * 
     * @param group the parent group to count for
     * @return child contacts count for the given group
     */
    public int countChildContacts(MetaContactGroup group)
    {
        if (showOffline)
            return group.countChildContacts();
        else
        {
            int count = 0;
            Iterator i = group.getChildContacts();

            while (i.hasNext())
            {
                MetaContact metaContact = (MetaContact) i.next();

                if (isContactOnline(metaContact))
                {
                    count++;
                }
                else
                {
                    break;
                }
            }
            return count;
        }
    }

    /**
     * Checks if the given group contains online contacts.
     * 
     * @param group the group to check for online contacts
     * @return TRUE if the given group contains online contacts, FALSE otherwise
     */
    private boolean containsOnlineContacts(MetaContactGroup group)
    {
        Iterator childContacts = group.getChildContacts();
        while (childContacts.hasNext())
        {
            MetaContact contact = (MetaContact) childContacts.next();

            if (isContactOnline(contact))
                return true;
        }

        return false;
    }

    /**
     * Adds the given <tt>MetaContact</tt> to the list of active contacts.
     * 
     * @param metaContact the <tt>MetaContact</tt> to add.
     */
    public void addActiveContact(MetaContact metaContact)
    {
        synchronized (activeContacts)
        {
            if (!activeContacts.contains(metaContact))
                this.activeContacts.add(metaContact);
        }
    }

    /**
     * Removes the given <tt>MetaContact</tt> from the list of active
     * contacts.
     * 
     * @param metaContact the <tt>MetaContact</tt> to remove.
     */
    public void removeActiveContact(MetaContact metaContact)
    {
        synchronized (activeContacts)
        {
            if (activeContacts.contains(metaContact))
                this.activeContacts.remove(metaContact);
        }
    }

    /**
     * Removes all contacts from the list of active contacts.
     */
    public void removeAllActiveContacts()
    {
        synchronized (activeContacts)
        {
            if (activeContacts.size() > 0)
            {
                this.activeContacts.removeAllElements();
            }
        }
    }

    /**
     * Checks if the given contact is currently active.
     * 
     * @param metaContact the <tt>MetaContact</tt> to verify
     * @return TRUE if the given <tt>MetaContact</tt> is active, FALSE -
     *         otherwise
     */
    public boolean isContactActive(MetaContact metaContact)
    {
        synchronized (activeContacts)
        {
            return this.activeContacts.contains(metaContact);
        }
    }

    public int getCount()
    {
        return rootGroup.countChildContacts();
    }

    public Object getItem(int position)
    {
        return rootGroup.getMetaContact(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public TextView getGenericView()
    {
        // Layout parameters for the ExpandableListView
        ViewGroup.MarginLayoutParams lp =
            new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT, 64);

        TextView textView = new TextView(androidContext);
        textView.setLayoutParams(lp);
        // Center the text vertically
        textView.setGravity(Gravity.CENTER_VERTICAL);
        // Set the text starting position
        textView.setPadding(36, 0, 0, 0);
        return textView;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        String contactName = ((MetaContact) getItem(position)).getDisplayName();

        int mailIndex = contactName.indexOf("@");
        contactName =
            (mailIndex > 0) ? contactName.substring(0, mailIndex) : contactName;

        if (convertView != null)
        {
            if (convertView instanceof TextView)
            {
                ((TextView) convertView).setText(contactName);
            }
            else
                ;
        }
        else
        {
            TextView textView = getGenericView();
            textView.setText(contactName);
            convertView = textView;
        }
        return convertView;
    }
}
