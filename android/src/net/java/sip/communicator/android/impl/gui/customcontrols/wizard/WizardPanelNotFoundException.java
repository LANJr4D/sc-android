package net.java.sip.communicator.android.impl.gui.customcontrols.wizard;
/**
 * A <tt>RuntimeException</tt>, which is thrown if the wizard doesn't find
 * the panel corresponding to a given <tt>WizardPanelDescriptor</tt>.
 * 
 * @author Yana Stamcheva
 */
public class WizardPanelNotFoundException extends RuntimeException {
        
    public WizardPanelNotFoundException() {
        super();
    }
}