/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.account;

import net.java.sip.communicator.android.impl.gui.i18n.Messages;
import net.java.sip.communicator.android.service.gui.WizardPage;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * The <tt>NoAccountFoundPage</tt> is the page shown in the account
 * registration wizard shown in the beginning of the program, when no registered
 * accounts are found.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class NoAccountFoundPage
    extends LinearLayout
    implements WizardPage
{
    private static String NO_ACCOUNT_FOUND_PAGE = "NoAccountFoundPage";

    /**
     * Creates an instance of <tt>NoAccountFoundPage</tt>.
     */
    public NoAccountFoundPage(Context androidContext)
    {
        super(androidContext);
        setPreferredHeight(LayoutParams.FILL_PARENT);
        setPreferredWidth(LayoutParams.FILL_PARENT);

        final TextView messageLabel = new TextView(androidContext);
        messageLabel
            .setText(Messages.getI18NString("noAccountFound").getText());
        

        this.addView(messageLabel, new LinearLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }

    /**
     * Implements the <tt>WizardPage.getIdentifier</tt> method. Returns the
     * identifier of this page.
     */
    public Object getIdentifier()
    {
        return NO_ACCOUNT_FOUND_PAGE;
    }

    /**
     * Implements the <tt>WizardPage.getNextPageIdentifier</tt> method.
     * Returns the identifier of the default wizard page.
     */
    public Object getNextPageIdentifier()
    {
        return WizardPage.DEFAULT_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <tt>WizardPage.getBackPageIdentifier</tt> method.
     * Returns null to identify that this is the first wizard page and this way
     * to disable the "Back" wizard button.
     */
    public Object getBackPageIdentifier()
    {
        return null;
    }

    /**
     * Implements the <tt>WizardPage.getWizardForm</tt> method. Returns this
     * panel.
     */
    public Object getWizardForm()
    {
        return this;
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    public void pageShowing()
    {
    }

    public void pageNext()
    {
    }

    public void pageBack()
    {
    }
}
