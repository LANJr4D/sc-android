package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.*;

import net.java.sip.communicator.service.contactlist.*;
import android.content.*;
import android.database.*;
import android.view.*;
import android.widget.*;

/**
 * Defines the view of the comboBox that displays the list of groups from the
 * user accounts.
 * 
 * @author cristina
 * 
 */
public class GroupComboAdapter
    implements SpinnerAdapter
{
    /**
     * Collection of groups belonging to user accounts
     */
    private Vector groups;

    private Context androidContext;

    public GroupComboAdapter(Iterator groups, Context androidContext)
    {
        this.androidContext = androidContext;

        this.groups = new Vector();
        while (groups.hasNext())
        {
            this.groups.add(groups.next());
        }
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        String groupName =
            ((MetaContactGroup) groups.elementAt(position)).getGroupName();

        if (convertView == null)
        {
            convertView = new TextView(androidContext);
            ((TextView) convertView).setText(groupName);
        }
        else
        {
            ((TextView) convertView).setText(groupName);
        }
        return convertView;
    }

    public View getMeasurementView(ViewGroup arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public int getCount()
    {
        return groups.size();
    }

    public Object getItem(int position)
    {
        return groups.elementAt(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public int getNewSelectionForKey(int currentSelection, int keyCode,
        KeyEvent event)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        String groupName =
            ((MetaContactGroup) groups.elementAt(position)).getGroupName();

        if (convertView == null)
        {
            convertView = new TextView(androidContext);
            ((TextView) convertView).setText(groupName);
        }
        else
        {
            ((TextView) convertView).setText(groupName);
        }
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer)
    {
        // TODO Auto-generated method stub

    }

    public boolean stableIds()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public void unregisterDataSetObserver(DataSetObserver arg0)
    {
        // TODO Auto-generated method stub

    }

}
