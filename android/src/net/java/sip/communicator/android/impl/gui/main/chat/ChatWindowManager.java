package net.java.sip.communicator.android.impl.gui.main.chat;

import java.io.*;
import java.util.*;

import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.android.impl.gui.utils.*;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.event.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;

/**
 * Manages chat windows and panels.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class ChatWindowManager
    extends LinearLayout
{

    private Logger logger = Logger.getLogger(ChatWindowManager.class);

    /**
     * Parallel collections that maintain a list of chats with their
     * corresponding contacts
     */
    Vector chatWindows = new Vector();

    Vector chatContacts = new Vector();

    private MainFrame mainFrame;

    private Object syncChat = new Object();

    private LinearLayout headerLayout;

    private LinearLayout contentLayout;

    private LinearLayout contactLayout;

    private ImageButton nextButton;

    private ImageButton prevButton;

    private ImageView closeView;

    private TextView noChats;

    private static final String NEXT_IMAGE =
        "/resources/images/impl/gui/buttons/next.png";

    private static final String PREVIOUS_IMAGE =
        "/resources/images/impl/gui/buttons/previous.png";

    private static final String CLOSE_IMAGE =
        "/resources/images/impl/gui/buttons/close.png";

    /*
     * Index of the currently displayed chat
     */
    private int currentChatPosition;

    private TextView contactInfoView;

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);

    public ChatWindowManager(MainFrame mainFrame)
    {
        super(mainFrame.getAndroidContext());

        try
        {
            this.mainFrame = mainFrame;
            this.currentChatPosition = -1;

            // design layout
            Context androidContext = mainFrame.getAndroidContext();
            this.setOrientation(LinearLayout.VERTICAL);

            headerLayout = new LinearLayout(androidContext);
            headerLayout.setOrientation(LinearLayout.HORIZONTAL);

            // chat window contact title
            contactLayout = new LinearLayout(androidContext);
            contactLayout.setOrientation(LinearLayout.HORIZONTAL);
            contactLayout.setBackgroundColor(Color.rgb(153, 204, 255));

            contactInfoView = new TextView(androidContext);
            contactInfoView.setText("Contact");
            contactInfoView.setTextColor(Color.DKGRAY);

            closeView = new ImageView(androidContext);
            closeView.setImageBitmap(BitmapFactory
                .decodeStream(getIconForPath(CLOSE_IMAGE)));
            closeView.setPadding(10, 0, 0, 0);
            closeView.setOnClickListener(new OnClickListener()
            {
                public void onClick(View arg0)
                {
                    closeChat();
                }

            });

            contactLayout.addView(contactInfoView, WRAP_CONTENT);

            headerLayout.addView(contactLayout, WRAP_CONTENT);

            LinearLayout buttonLayout = new LinearLayout(androidContext);
            buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
            buttonLayout.setPadding(50, 0, 0, 0);

            nextButton = new ImageButton(androidContext);
            nextButton.setImageBitmap(BitmapFactory
                .decodeStream(getIconForPath(NEXT_IMAGE)));
            nextButton.setPadding(5, 0, 20, 0);
            // nextButton.setBackgroundColor(Color.BLACK);
            // nextButton.setTextColor(Color.WHITE);
            nextButton.setEnabled(false);
            nextButton.setOnClickListener(new OnClickListener()
            {
                public void onClick(View arg0)
                {
                    openChat((MetaContact) chatContacts
                        .elementAt(currentChatPosition + 1));
                }
            });

            prevButton = new ImageButton(androidContext);
            prevButton.setImageBitmap(BitmapFactory
                .decodeStream(getIconForPath(PREVIOUS_IMAGE)));
            prevButton.setPadding(20, 0, 5, 0);
            prevButton.setEnabled(false);
            // prevButton.setBackgroundColor(Color.BLACK);
            // prevButton.setTextColor(Color.WHITE);
            prevButton.setOnClickListener(new OnClickListener()
            {
                public void onClick(View arg0)
                {
                    openChat((MetaContact) chatContacts
                        .elementAt(currentChatPosition - 1));
                }
            });

            buttonLayout.addView(prevButton, WRAP_CONTENT);
            buttonLayout.addView(nextButton, WRAP_CONTENT);

            headerLayout.addView(buttonLayout, WRAP_CONTENT);

            noChats = new TextView(androidContext);
            noChats.setText("No contacts to chat with");

            contentLayout = new LinearLayout(androidContext);
            contentLayout.setOrientation(LinearLayout.VERTICAL);
            contentLayout.addView(noChats, WRAP_CONTENT);

            this.addView(headerLayout, WRAP_CONTENT);
            this.addView(contentLayout, WRAP_CONTENT);
        }
        catch (Exception e)
        {
            logger.error("Error new chatWindowManager", e);
        }
    }

    void closeChat()
    {
        // remove chat and corresponding contact from reference vectors
        synchronized (chatContacts)
        {
            chatContacts.remove(currentChatPosition);
        }

        int remainingChats = 0;
        synchronized (chatWindows)
        {
            chatWindows.remove(currentChatPosition);
            remainingChats = chatWindows.size();
        }

        // set current chat window
        if (remainingChats > 0)
        {
            // if the closed chat window was the last in the list, update
            // currentChatPosition to the previous index
            currentChatPosition =
                (currentChatPosition == remainingChats) ? currentChatPosition - 1
                    : currentChatPosition;

            // set next chat as current chat
            openChat((MetaContact) chatContacts.elementAt(currentChatPosition));
        }
        else
        {
            currentChatPosition = -1;

            // no chats to display
            UIThreadUtilities.runOnUIThread(this, new Runnable()
            {

                public void run()
                {
                    // update contactInfo
                    contactInfoView.setText("Contact");
                    contactLayout.removeAllViews();
                    contactLayout.addView(contactInfoView, 0, WRAP_CONTENT);

                    // set noChats
                    contentLayout.removeAllViews();
                    contentLayout.addView(noChats, WRAP_CONTENT);

                    // update navigation button appearance
                    setNavigBtnsAccToChatIndex(currentChatPosition);

                    headerLayout.invalidate();
                    contentLayout.invalidate();
                }
            });
        }
    }

    /**
     * Opens a the specified chatPanel and brings it to the front if so
     * specified.
     * 
     * @param metaContact the metaContact to whom this chat window corresponds
     */
    public void openChat(final MetaContact metaContact)
    {
        if (!isChatOpenedForContact(metaContact))
        {
            UIThreadUtilities.runOnUIThread(this, new Runnable()
            {

                public void run()
                {
                    // get chat window and display it
                    ChatWindow contactChat = getContactChat(metaContact);
                    contactChat.setShown(true);

                    currentChatPosition = getChatIndex(metaContact);

                    // update contactInfo
                    contactInfoView.setText(GuiUtils
                        .formatContactName(metaContact.getDisplayName()));
                    contactLayout.removeViewAt(0);
                    contactLayout.addView(contactInfoView, 0, WRAP_CONTENT);
                    if (contactLayout.indexOfChild(closeView) == -1)
                    {
                        contactLayout.addView(closeView, WRAP_CONTENT);
                    }

                    // set chat window as current view
                    View previousChatWindow = contentLayout.getChildAt(0);
                    if (previousChatWindow instanceof ChatWindow)
                    {
                        ((ChatWindow) previousChatWindow).setShown(false);
                    }
                    contentLayout.removeAllViews();
                    contentLayout.addView(contactChat, WRAP_CONTENT);

                    // update navigation button appearence
                    setNavigBtnsAccToChatIndex(currentChatPosition);

                    contactInfoView.invalidate();
                    headerLayout.invalidate();
                    contentLayout.invalidate();
                }
            });
        }
    }

    /**
     * Opens a chat when receiving a messageReceived event from a contact that
     * hasn't a chat window of its own
     * 
     * @param metaContact
     * @param evt
     */
    public void openChatForReceivedMessage(final MetaContact metaContact,
        final MessageReceivedEvent evt, final String messageType)
    {
        UIThreadUtilities.runOnUIThread(this, new Runnable()
        {

            public void run()
            {
                // get chat window and display it
                ChatWindow contactChat = createChat(metaContact);
                contactChat.setShown(true);

                currentChatPosition = getChatIndex(metaContact);

                // update contactInfo
                contactInfoView.setText(GuiUtils.formatContactName(metaContact
                    .getDisplayName()));
                contactLayout.removeViewAt(0);
                contactLayout.addView(contactInfoView, 0, WRAP_CONTENT);
                if (contactLayout.indexOfChild(closeView) == -1)
                {
                    contactLayout.addView(closeView, WRAP_CONTENT);
                }

                // set chat window as current view
                View previousChatWindow = contentLayout.getChildAt(0);
                if (previousChatWindow instanceof ChatWindow)
                {
                    ((ChatWindow) previousChatWindow).setShown(false);
                }
                contentLayout.removeAllViews();
                contentLayout.addView(contactChat, WRAP_CONTENT);

                // update navigation button appearence
                setNavigBtnsAccToChatIndex(currentChatPosition);

                contactInfoView.invalidate();
                headerLayout.invalidate();
                contentLayout.invalidate();

                // process received message
                contactChat.processMessage(GuiUtils.formatContactName(evt
                    .getSourceContact().getDisplayName()), evt.getTimestamp(),
                    messageType, evt.getSourceMessage().getContent(), evt
                        .getSourceMessage().getContentType());
            }
        });
    }

    private int getChatIndex(MetaContact metaContact)
    {
        synchronized (chatContacts)
        {
            return chatContacts.indexOf(metaContact);
        }
    }

    /**
     * Enables/disables navigation buttons according to current displayed chat
     * window.
     * 
     * @param chatIndex index of the current chat window in chatWindows
     *            collection
     */
    private void setNavigBtnsAccToChatIndex(int chatIndex)
    {
        int chatNo = chatWindows.size();

        if (chatIndex == 0)
        {
            prevButton.setEnabled(false);
            nextButton.setEnabled(chatNo > 1);
        }

        if (chatIndex == (chatNo - 1))
        {
            prevButton.setEnabled(chatNo > 1);
            nextButton.setEnabled(false);
        }

        if ((chatIndex > 0) && (chatIndex < (chatNo - 1)))
        {
            prevButton.setEnabled(true);
            nextButton.setEnabled(true);
        }
    }

    /**
     * Returns TRUE if this chat window contains a chat for the given contact,
     * FALSE otherwise.
     * 
     * @param key the key, which corresponds to the chat we are looking for. It
     *            could be a <tt>MetaContact</tt> in the case of single user
     *            chat and a <tt>ChatRoom</tt> in the case of a multi user
     *            chat
     * @return TRUE if this chat window contains a chat corresponding to the
     *         given key, FALSE otherwise
     */
    public boolean containsChat(Object key)
    {
        synchronized (chatContacts)
        {
            int contactIndex = chatContacts.indexOf(key);
            return (contactIndex > -1);
        }
    }

    /**
     * Returns TRUE if there is an opened <tt>ChatPanel</tt> for the given
     * <tt>MetaContact</tt>.
     * 
     * @param metaContact the <tt>MetaContact</tt>, for which the chat is
     *            about
     * @return TRUE if there is an opened <tt>ChatPanel</tt> for the given
     *         <tt>MetaContact</tt>
     */
    public boolean isChatOpenedForContact(MetaContact metaContact)
    {
        synchronized (syncChat)
        {
            if (containsChat(metaContact) && getChat(metaContact).isShown())
                return true;

            return false;
        }
    }

    /**
     * Returns the <tt>ChatPanel</tt> corresponding to the given meta contact.
     * 
     * @param metaContact the key, which corresponds to the chat we are looking
     *            for. It could be a <tt>MetaContact</tt> in the case of
     *            single user chat and a <tt>ChatRoom</tt> in the case of a
     *            multi user chat
     * @return the <tt>ChatPanel</tt> corresponding to the given meta contact
     */
    public ChatWindow getChat(Object metaContact)
    {
        int contactIndex = 0;

        synchronized (chatContacts)
        {
            contactIndex = chatContacts.indexOf(metaContact);
        }

        synchronized (chatWindows)
        {
            if (contactIndex > -1)
            {
                return (ChatWindow) chatWindows.elementAt(contactIndex);
            }
            else
                return null;
        }
    }

    /**
     * Returns the chat window corresponding to the given meta contact
     * 
     * @param metaContact the meta contact.
     * @return the chat window corresponding to the given meta contact
     */
    public ChatWindow getContactChat(MetaContact metaContact)
    {
        synchronized (syncChat)
        {
            if (containsChat(metaContact))
            {
                return (ChatWindow) getChat(metaContact);
            }
            else
            {
                return createChat(metaContact);
            }
        }
    }

    private ChatWindow createChat(MetaContact metaContact)
    {
        ChatWindow newChatWindow =
            new ChatWindow(mainFrame.getAndroidContext(), metaContact);
        chatWindows.addElement(newChatWindow);
        chatContacts.addElement(metaContact);

        return newChatWindow;
    }

    /**
     * Gtes the icon input stream corresponding to the file having the indicated
     * path.
     * 
     * @param path of the icon file
     * @return InputStream object representing the requested icon
     */
    private InputStream getIconForPath(String path)
    {
        return ChatWindowManager.class.getClassLoader().getResourceAsStream(
            path);
    }

}
