/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main.contactlist;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.android.impl.gui.main.chat.*;
import net.java.sip.communicator.android.impl.gui.utils.*;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.service.protocol.event.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.widget.*;

/**
 * The contactlist panel not only contains the contact list but it has the role
 * of a message dispatcher. It process all sent and received messages as well as
 * all typing notifications. Here are managed all contact list mouse events.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class ContactListPanel
    extends LinearLayout
    implements MessageListener, TypingNotificationsListener,
    ContactListListener
{

    private MainFrame mainFrame;

    private ContactList contactList;

    private Logger logger = Logger.getLogger(ContactListPanel.class);

    private static LinearLayout.LayoutParams WRAP_CONTENT_LP =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates the contactlist scroll panel defining the parent frame.
     * 
     * @param mainFrame The parent frame.
     */
    public ContactListPanel(MainFrame mainFrame)
    {
        super(mainFrame.getAndroidContext());
        this.setOrientation(LinearLayout.VERTICAL);

        Context androidContext = this.mContext;

        this.mainFrame = mainFrame;
    }

    /**
     * Initializes the contact list.
     * 
     * @param contactListService The MetaContactListService which will be used
     *            for a contact list data model.
     */
    public void initList(MetaContactListService contactListService)
    {
        this.contactList = new ContactList(mainFrame);
        this.addView(contactList, WRAP_CONTENT_LP);

        this.contactList.addContactListListener(this);
    }

    /**
     * Returns the contact list.
     * 
     * @return the contact list
     */
    public ContactList getContactList()
    {
        return this.contactList;
    }

    /**
     * Implements the ContactListListener.contactSelected method.
     */
    public void contactClicked(ContactListEvent evt)
    {
        MetaContact metaContact = evt.getSourceContact();

        // Searching for the right proto contact to use as default for the
        // chat conversation.
        Contact defaultContact = metaContact.getDefaultContact();

        ProtocolProviderService defaultProvider =
            defaultContact.getProtocolProvider();

        OperationSetBasicInstantMessaging defaultIM =
            (OperationSetBasicInstantMessaging) defaultProvider
                .getOperationSet(OperationSetBasicInstantMessaging.class);

        ProtocolProviderService protoContactProvider;
        OperationSetBasicInstantMessaging protoContactIM;

        if (defaultContact.getPresenceStatus().getStatus() < 1
            && (!defaultIM.isOfflineMessagingSupported() || !defaultProvider
                .isRegistered()))
        {
            Iterator<Contact> protoContacts = metaContact.getContacts();

            while (protoContacts.hasNext())
            {
                Contact contact = protoContacts.next();

                protoContactProvider = contact.getProtocolProvider();

                protoContactIM =
                    (OperationSetBasicInstantMessaging) protoContactProvider
                        .getOperationSet(OperationSetBasicInstantMessaging.class);

                if (protoContactIM.isOfflineMessagingSupported()
                    && protoContactProvider.isRegistered())
                {
                    defaultContact = contact;
                }
            }
        }
    }

    /**
     * Implements the ContactListListener.groupSelected method.
     */
    public void groupSelected(ContactListEvent evt)
    {
    }

    /**
     * When a message is received determines whether to open a new chat window
     * or chat window tab, or to indicate that a message is received from a
     * contact which already has an open chat. When the chat is found checks if
     * in mode "Auto popup enabled" and if this is the case shows the message in
     * the appropriate chat panel.
     * 
     * @param evt the event containing details on the received message
     */
    public void messageReceived(MessageReceivedEvent evt)
    {
        logger.trace("MESSAGE RECEIVED from contact: "
            + evt.getSourceContact().getAddress());

        Contact protocolContact = evt.getSourceContact();
        Date date = evt.getTimestamp();
        Message message = evt.getSourceMessage();
        int eventType = evt.getEventType();

        MetaContact metaContact =
            mainFrame.getContactList()
                .findMetaContactByContact(protocolContact);

        if (metaContact != null)
        {
            try
            {
                // Obtain the corresponding chat panel.

                // Distinguish the message type, depending on the type of event
                // that
                // we have received.
                String messageType = null;

                if (eventType == MessageReceivedEvent.CONVERSATION_MESSAGE_RECEIVED)
                {
                    messageType = Constants.INCOMING_MESSAGE;
                }
                else if (eventType == MessageReceivedEvent.SYSTEM_MESSAGE_RECEIVED)
                {
                    messageType = Constants.SYSTEM_MESSAGE;
                }
                else if (eventType == MessageReceivedEvent.SMS_MESSAGE_RECEIVED)
                {
                    messageType = Constants.SMS_MESSAGE;
                }

                // Opens the chat panel with the new message.
                mainFrame.displayChatWindowManager();

                ChatWindowManager chatWindowManager =
                    mainFrame.getChatWindowManager();

                if (!chatWindowManager.containsChat(metaContact))
                {
                    chatWindowManager.openChatForReceivedMessage(metaContact,
                        evt, messageType);
                }
                else
                {
                    chatWindowManager.getChat(metaContact).processMessage(
                        GuiUtils.formatContactName(protocolContact
                            .getDisplayName()), date, messageType,
                        message.getContent(), message.getContentType());
                }
            }
            catch (Exception e)
            {
                logger.error("Error open chat panel when message received", e);
            }
        }
        else
        {
            logger.trace("MetaContact not found for protocol contact: "
                + protocolContact + ".");
        }
    }

    /**
     * When a sent message is delivered shows it in the chat conversation panel.
     * 
     * @param evt the event containing details on the message delivery
     */
    public void messageDelivered(MessageDeliveredEvent evt)
    {
        Contact contact = evt.getDestinationContact();

        MetaContact metaContact =
            mainFrame.getContactList().findMetaContactByContact(contact);

        logger.trace("MESSAGE DELIVERED to contact: "
            + evt.getDestinationContact().getAddress());

        Message msg = evt.getSourceMessage();

        // TODO: process message
    }

    /**
     * Shows a warning message to the user when message delivery has failed.
     * 
     * @param evt the event containing details on the message delivery failure
     */
    public void messageDeliveryFailed(MessageDeliveryFailedEvent evt)
    {
        String errorMsg = null;

        Message sourceMessage = (Message) evt.getSource();

        Contact sourceContact = evt.getDestinationContact();

        MetaContact metaContact =
            mainFrame.getContactList().findMetaContactByContact(sourceContact);

        if (evt.getErrorCode() == MessageDeliveryFailedEvent.OFFLINE_MESSAGES_NOT_SUPPORTED)
        {

            errorMsg =
                Messages.getI18NString("msgDeliveryOfflineNotSupported")
                    .getText();
        }
        else if (evt.getErrorCode() == MessageDeliveryFailedEvent.NETWORK_FAILURE)
        {

            errorMsg = Messages.getI18NString("msgNotDelivered").getText();
        }
        else if (evt.getErrorCode() == MessageDeliveryFailedEvent.PROVIDER_NOT_REGISTERED)
        {

            errorMsg =
                Messages.getI18NString("msgSendConnectionProblem").getText();
        }
        else if (evt.getErrorCode() == MessageDeliveryFailedEvent.INTERNAL_ERROR)
        {

            errorMsg =
                Messages.getI18NString("msgDeliveryInternalError").getText();
        }
        else
        {
            errorMsg =
                Messages.getI18NString("msgDeliveryFailedUnknownError")
                    .getText();
        }

        ChatWindow chatWindow =
            mainFrame.getChatWindowManager().getChat(metaContact);

        chatWindow.processMessage(metaContact.getDisplayName(), new Date(System
            .currentTimeMillis()), Constants.OUTGOING_MESSAGE, sourceMessage
            .getContent(), sourceMessage.getContentType());

        chatWindow.processMessage(metaContact.getDisplayName(), new Date(System
            .currentTimeMillis()), Constants.ERROR_MESSAGE, errorMsg, "text");
    }

    public void typingNotificationReceifed(TypingNotificationEvent arg0)
    {
        // TODO Auto-generated method stub
    }

    public void protocolContactClicked(ContactListEvent evt)
    {
        // TODO Auto-generated method stub

    }

}
