/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main.contactlist;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.contactlist.event.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.view.*;
import android.widget.*;

/**
 * The <tt>ContactList</tt> is a JList that represents the contact list. A
 * custom data model and a custom list cell renderer is used. This class manages
 * all meta contact list events, like <code>metaContactAdded</code>,
 * <code>metaContactMoved</code>, <code>metaContactGroupAdded</code>, etc.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class ContactList
    extends LinearLayout
    implements MetaContactListListener
{

    private static final String ADD_OPERATION = "AddOperation";

    private static final String REMOVE_OPERATION = "RemoveOperation";

    private static final String MODIFY_OPERATION = "ModifyOperation";

    private Logger logger = Logger.getLogger(ContactList.class.getName());

    private MetaContactListService contactList;

    private Vector contactListListeners = new Vector();

    private Vector excContactListListeners = new Vector();

    private MainFrame mainFrame;

    private Hashtable contentToRefresh = new Hashtable();

    private boolean refreshEnabled = true;

    /**
     * List displaying contacts that are part of groups
     */
    private ExpandableListView groupListView;

    private GroupListModel groupListModel;

    /**
     * List displaying contacts that are not part of a group
     */
    private ListView contactListView;

    private ContactListModel contactListModel;

    private static final LinearLayout.LayoutParams LINEAR_WRAP_CONTENT_LAYOUT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of the <tt>ContactList</tt>.
     * 
     * @param mainFrame The main application window.
     */
    public ContactList(MainFrame mainFrame)
    {
        super(mainFrame.getAndroidContext());

        this.setOrientation(LinearLayout.VERTICAL);

        this.mainFrame = mainFrame;

        this.contactList = mainFrame.getContactList();
        this.contactList.addMetaContactListListener(this);

        Context androidContext = mainFrame.getAndroidContext();

        groupListView = new ExpandableListView(androidContext);
        groupListModel = new GroupListModel(contactList, androidContext);
        groupListView.setAdapter(groupListModel);
        groupListView
            .setOnChildClickListener(new GroupListOnChildClickListener());

        contactListView = new ListView(androidContext);
        contactListModel = new ContactListModel(contactList, androidContext);
        contactListView.setAdapter(contactListModel);
        contactListView
            .setOnItemClickListener(new ContactListOnItemClickListener());

        this.addView(contactListView, LINEAR_WRAP_CONTENT_LAYOUT);
        this.addView(groupListView, LINEAR_WRAP_CONTENT_LAYOUT);

        new ContactListRefresh().start();

    }

    /**
     * Handles the <tt>MetaContactEvent</tt>. Refreshes the list model.
     */
    public void metaContactAdded(MetaContactEvent evt)
    {
        this.addContact(evt.getSourceMetaContact());
    }

    /**
     * Handles the <tt>MetaContactRenamedEvent</tt>. Refreshes the list when
     * a meta contact is renamed.
     */
    public void metaContactRenamed(MetaContactRenamedEvent evt)
    {
        this.refreshContact(evt.getSourceMetaContact());
    }

    /**
     * Handles the <tt>ProtoContactEvent</tt>. Refreshes the list when a
     * protocol contact has been added.
     */
    public void protoContactAdded(ProtoContactEvent evt)
    {
        MetaContact parentMetaContact = evt.getNewParent();

        this.refreshContact(parentMetaContact);
    }

    /**
     * Handles the <tt>ProtoContactEvent</tt>. Refreshes the list when a
     * protocol contact has been removed.
     */
    public void protoContactRemoved(ProtoContactEvent evt)
    {
        this.refreshContact(evt.getOldParent());
    }

    /**
     * Handles the <tt>ProtoContactEvent</tt>. Refreshes the list when a
     * protocol contact has been moved.
     */
    public void protoContactMoved(ProtoContactEvent evt)
    {
        MetaContact oldParentMetaContact = evt.getOldParent();
        MetaContact newParentMetaContact = evt.getNewParent();

        this.refreshContact(oldParentMetaContact);
        this.refreshContact(newParentMetaContact);
    }

    /**
     * Handles the <tt>MetaContactEvent</tt>. Refreshes the list when a meta
     * contact has been removed.
     */
    public void metaContactRemoved(MetaContactEvent evt)
    {
        this.removeContact(evt);
    }

    /**
     * Handles the <tt>MetaContactMovedEvent</tt>. Refreshes the list when a
     * meta contact has been moved.
     */
    public void metaContactMoved(MetaContactMovedEvent evt)
    {
        this.modifyGroup(evt.getNewParent());
        this.modifyGroup(evt.getOldParent());
    }

    /**
     * Handles the <tt>MetaContactGroupEvent</tt>. Refreshes the list model
     * when a new meta contact group has been added.
     */
    public void metaContactGroupAdded(MetaContactGroupEvent evt)
    {
        MetaContactGroup group = evt.getSourceMetaContactGroup();

        if (!group.equals(contactList.getRoot()))
            this.addGroup(group);
    }

    /**
     * Handles the <tt>MetaContactGroupEvent</tt>. Refreshes the list when a
     * meta contact group has been modified.
     */
    public void metaContactGroupModified(MetaContactGroupEvent evt)
    {
        MetaContactGroup group = evt.getSourceMetaContactGroup();

        if (!group.equals(contactList.getRoot()))
            this.modifyGroup(evt.getSourceMetaContactGroup());
    }

    /**
     * Handles the <tt>MetaContactGroupEvent</tt>. Refreshes the list when a
     * meta contact group has been removed.
     */
    public void metaContactGroupRemoved(MetaContactGroupEvent evt)
    {
        MetaContactGroup group = evt.getSourceMetaContactGroup();

        if (!group.equals(contactList.getRoot()))
            this.removeGroup(evt.getSourceMetaContactGroup());
    }

    /**
     * Handles the <tt>MetaContactGroupEvent</tt>. Refreshes the list model
     * when the contact list groups has been reordered. Moves the selection
     * index to the index of the contact that was selected before the reordered
     * event. This way the selection depends on the contact and not on the
     * index.
     */
    public void childContactsReordered(MetaContactGroupEvent evt)
    {
        this.modifyGroup(evt.getSourceMetaContactGroup());
    }

    /**
     * Returns the list of all groups.
     * 
     * @return The list of all groups.
     */
    public Iterator getAllGroups()
    {
        return contactList.getRoot().getSubgroups();
    }

    /**
     * Returns the Meta Contact Group corresponding to the given MetaUID.
     * 
     * @param metaUID An identifier of a group.
     * @return The Meta Contact Group corresponding to the given MetaUID.
     */
    public MetaContactGroup getGroupByID(String metaUID)
    {
        Iterator i = contactList.getRoot().getSubgroups();
        while (i.hasNext())
        {
            MetaContactGroup group = (MetaContactGroup) i.next();

            if (group.getMetaUID().equals(metaUID))
            {
                return group;
            }
        }
        return null;
    }

    /**
     * Adds a listener for <tt>ContactListEvent</tt>s.
     * 
     * @param listener the listener to add
     */
    public void addContactListListener(ContactListListener listener)
    {
        synchronized (contactListListeners)
        {
            if (!contactListListeners.contains(listener))
                this.contactListListeners.add(listener);
        }
    }

    /**
     * Removes a listener previously added with <tt>addContactListListener</tt>.
     * 
     * @param listener the listener to remove
     */
    public void removeContactListListener(ContactListListener listener)
    {
        synchronized (contactListListeners)
        {
            this.contactListListeners.remove(listener);
        }
    }

    /**
     * Adds a listener for <tt>ContactListEvent</tt>s.
     * 
     * @param listener the listener to add
     */
    public void addExcContactListListener(ContactListListener listener)
    {
        synchronized (excContactListListeners)
        {
            if (!excContactListListeners.contains(listener))
                this.excContactListListeners.add(listener);
        }
    }

    /**
     * Removes a listener previously added with <tt>addContactListListener</tt>.
     * 
     * @param listener the listener to remove
     */
    public void removeExcContactListListener(ContactListListener listener)
    {
        synchronized (excContactListListeners)
        {
            this.excContactListListeners.remove(listener);
        }
    }

    /**
     * Creates the corresponding ContactListEvent and notifies all
     * <tt>ContactListListener</tt>s that a contact is selected.
     * 
     * @param source the contact that this event is about.
     * @param eventID the id indicating the exact type of the event to fire.
     * @param clickCount the number of clicks accompanying the event.
     */
    public void fireContactListEvent(Object source, int eventID, int clickCount)
    {
        ContactListEvent evt =
            new ContactListEvent(source, eventID, clickCount);

        if (excContactListListeners.size() > 0)
        {
            synchronized (excContactListListeners)
            {
                Iterator listeners =
                    new Vector(this.excContactListListeners).iterator();

                while (listeners.hasNext())
                {
                    ContactListListener listener =
                        (ContactListListener) listeners.next();

                    switch (evt.getEventID())
                    {
                    case ContactListEvent.CONTACT_SELECTED:
                        listener.contactClicked(evt);
                        break;
                    case ContactListEvent.PROTOCOL_CONTACT_SELECTED:
                        listener.protocolContactClicked(evt);
                        break;
                    case ContactListEvent.GROUP_SELECTED:
                        listener.groupSelected(evt);
                        break;
                    default:
                        logger.error("Unknown event type " + evt.getEventID());
                    }
                }
            }
        }
        else
        {
            synchronized (contactListListeners)
            {
                Iterator listeners = this.contactListListeners.iterator();

                while (listeners.hasNext())
                {
                    ContactListListener listener =
                        (ContactListListener) listeners.next();
                    switch (evt.getEventID())
                    {
                    case ContactListEvent.CONTACT_SELECTED:
                        listener.contactClicked(evt);
                        break;
                    case ContactListEvent.PROTOCOL_CONTACT_SELECTED:
                        listener.protocolContactClicked(evt);
                        break;
                    case ContactListEvent.GROUP_SELECTED:
                        listener.groupSelected(evt);
                        break;
                    default:
                        logger.error("Unknown event type " + evt.getEventID());
                    }
                }
            }
        }
    }

    /**
     * Creates the corresponding ContactListEvent and notifies all
     * <tt>ContactListListener</tt>s that a contact is selected.
     * 
     * @param sourceContact the contact that this event is about
     * @param protocolContact the protocol contact the this event is about
     * @param eventID the id indicating the exact type of the event to fire.
     */
    public void fireContactListEvent(MetaContact sourceContact,
        Contact protocolContact, int eventID)
    {
        ContactListEvent evt =
            new ContactListEvent(sourceContact, protocolContact, eventID);

        synchronized (contactListListeners)
        {
            Iterator listeners = this.contactListListeners.iterator();

            while (listeners.hasNext())
            {
                ContactListListener listener =
                    (ContactListListener) listeners.next();
                switch (evt.getEventID())
                {
                case ContactListEvent.CONTACT_SELECTED:
                    listener.contactClicked(evt);
                    break;
                case ContactListEvent.PROTOCOL_CONTACT_SELECTED:
                    listener.protocolContactClicked(evt);
                    break;
                default:
                    logger.error("Unknown event type " + evt.getEventID());
                }
            }
        }
    }

    /**
     * Takes care of keeping the contact list up to date.
     */
    class ContactListRefresh
        extends Thread
    {
        public void run()
        {
            try
            {
                Map copyContentToRefresh = null;

                while (refreshEnabled)
                {

                    // when contentToRefresh gets modified it notifies this
                    // Thread and the refresh starts
                    synchronized (contentToRefresh)
                    {
                        if (contentToRefresh.isEmpty())
                            contentToRefresh.wait();

                        copyContentToRefresh = new Hashtable(contentToRefresh);
                        contentToRefresh.clear();
                    }

                    Iterator i = copyContentToRefresh.entrySet().iterator();

                    while (i.hasNext())
                    {
                        Map.Entry groupEntry = (Map.Entry) i.next();

                        String operation = (String) groupEntry.getValue();

                        Object o = groupEntry.getKey();

                        if (o instanceof MetaContactGroup)
                        {
                            updateGroupList();
                        }
                        else if ((o instanceof MetaContact)
                            || (o instanceof MetaContactEvent))
                        {
                            updateEntireContactList();
                        }
                    }
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Refreshes the given group content.
     * 
     * @param group the group to refresh
     */
    public void modifyGroup(MetaContactGroup group)
    {
        synchronized (contentToRefresh)
        {
            if (group != null
                && (!contentToRefresh.containsKey(group) || contentToRefresh
                    .get(group).equals(REMOVE_OPERATION)))
            {

                contentToRefresh.put(group, MODIFY_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Refreshes all the contact list.
     */
    public void addGroup(MetaContactGroup group)
    {
        synchronized (contentToRefresh)
        {
            if (group != null
                && (!contentToRefresh.containsKey(group) || contentToRefresh
                    .get(group).equals(REMOVE_OPERATION)))
            {

                contentToRefresh.put(group, ADD_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Refreshes all the contact list.
     */
    public void removeGroup(MetaContactGroup group)
    {
        synchronized (contentToRefresh)
        {
            if (group != null
                && (contentToRefresh.get(group) == null || !contentToRefresh
                    .get(group).equals(REMOVE_OPERATION)))
            {

                contentToRefresh.put(group, REMOVE_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Refreshes the given meta contact content.
     * 
     * @param contact the meta contact to refresh
     */
    public void refreshContact(MetaContact contact)
    {
        synchronized (contentToRefresh)
        {
            if (contact != null
                && !contentToRefresh.containsKey(contact)
                && !contentToRefresh.containsKey(contact
                    .getParentMetaContactGroup()))
            {

                contentToRefresh.put(contact, MODIFY_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Refreshes the whole contact list.
     */
    public void refreshAll()
    {
        this.modifyGroup(contactList.getRoot());
    }

    /**
     * Adds the given contact to the contact list.
     */
    public void addContact(MetaContact contact)
    {
        synchronized (contentToRefresh)
        {
            if (contact != null
                && !contentToRefresh.containsKey(contact)
                && !contentToRefresh.containsKey(contact
                    .getParentMetaContactGroup()))
            {

                contentToRefresh.put(contact, ADD_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Refreshes all the contact list.
     */
    public void removeContact(MetaContactEvent event)
    {
        MetaContact metaContact = event.getSourceMetaContact();

        synchronized (contentToRefresh)
        {
            if (metaContact != null && !contentToRefresh.contains(event))
            {
                contentToRefresh.put(event, REMOVE_OPERATION);
                contentToRefresh.notifyAll();
            }
        }
    }

    /**
     * Returns the main frame.
     * 
     * @return the main frame
     */
    public MainFrame getMainFrame()
    {
        return mainFrame;
    }

    public ExpandableListView getGroupListView()
    {
        return groupListView;
    }

    public ListView getContactListView()
    {
        return contactListView;
    }

    /**
     * Updates the panel with new information from the contactList
     */
    public void updateEntireContactList()
    {
        UIThreadUtilities.runOnUIThread(mainFrame.getView(), new Runnable()
        {
            public void run()
            {
                contactListView.setAdapter(contactListModel);
                groupListView.setAdapter(groupListModel);
            }
        });

    }

    /**
     * Updates the panel with new information from the contactList
     */
    public void updateGroupList()
    {
        UIThreadUtilities.runOnUIThread(mainFrame.getView(), new Runnable()
        {
            public void run()
            {
                groupListView.setAdapter(groupListModel);
            }
        });

    }

    /**
     * Class dealing with user selections on contactListView
     * 
     */
    class ContactListOnItemClickListener
        implements AdapterView.OnItemClickListener
    {

        public void onItemClick(AdapterView arg0, View clickedView,
            int position, long id)
        {
            MetaContact metaContact =
                (MetaContact) contactListModel.getItem(position);

            // open chat window corresponding to the clicked contact
            mainFrame.displayChatWindowManager();

            mainFrame.getChatWindowManager().openChat(metaContact);
        }
    }

    /**
     * Class dealing with user selections on groupListView
     * 
     */
    class GroupListOnChildClickListener
        implements ExpandableListView.OnChildClickListener
    {

        public boolean onChildClick(ExpandableListView parentView, View arg1,
            int groupPosition, int childPosition, long id)
        {
            MetaContact metaContact =
                (MetaContact) groupListModel.getChild(groupPosition,
                    childPosition);

            // open chat window corresponding to the clicked contact
            mainFrame.displayChatWindowManager();

            mainFrame.getChatWindowManager().openChat(metaContact);

            return true;
        }

    }
}
