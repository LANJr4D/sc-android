/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.account;

import java.util.*;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.android.service.gui.event.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.graphics.*;
import android.widget.*;

/**
 * The <tt>AccountRegFirstPage</tt> is the first page of the account
 * registration wizard. This page contains a list of all registered
 * <tt>AccountRegistrationWizard</tt>s.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AccountRegFirstPage
    extends RadioGroup
    implements AccountRegistrationListener, WizardPage
{
    private Logger logger = Logger.getLogger(AccountRegFirstPage.class);

    private Object nextPageIdentifier;

    private AccountRegWizardContainerImpl wizardContainer;

    /**
     * Set of protocolXXX account registration wizzards available
     */
    private Vector wizardList;

    /**
     * For each available account registration wizard we build a radio button
     * that the user may select to access that specific wizzard. Each
     * RadioButton of the group will be set an ID that corresponds to the wizard
     * index in wizardList
     */
    private int lastRadioButtonId;

    private static final RadioGroup.LayoutParams RADIO_WRAP_CONTENT_LAYOUT =
        new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    public AccountRegFirstPage(AccountRegWizardContainerImpl container,
        Context androidContext)
    {
        super(androidContext);
        this.setOrientation(RadioGroup.VERTICAL);

        this.wizardContainer = container;
        this.wizardList = new Vector();

        // When a value is selected enables the "Next" wizard button
        this.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            public void onCheckedChanged(RadioGroup radiogroup, int i)
            {
                if (!wizardContainer.isNextFinishButtonEnabled())
                    wizardContainer.setNextFinishButtonEnabled(true);
            }

        });

        lastRadioButtonId = -1;
    }

    /**
     * When an <tt>AccountRegistrationWizard</tt> has been added to the
     * <tt>AccountRegistrationWizardContainer</tt> adds a line for this wizard
     * in the table.
     */
    public void accountRegistrationAdded(AccountRegistrationEvent event)
    {

        final AccountRegistrationWizard wizard =
            (AccountRegistrationWizard) event.getSource();

        Bitmap protocolImage = null;
        byte[] wizardIcon = wizard.getIcon();
        protocolImage =
            BitmapFactory.decodeByteArray(wizardIcon, 0, wizardIcon.length);

        Context androidContext = this.mContext;

        final RadioButton wizardOption = new RadioButton(androidContext);
        wizardOption.setId(++lastRadioButtonId);
        wizardOption.setText(wizard.getProtocolName());

        // TODO ad image
        // final ImageView registrationIcon = new ImageView(androidContext);
        // registrationIcon.setImageBitmap(protocolImage);

        // TODO add protocol description
        this.addView(wizardOption, RADIO_WRAP_CONTENT_LAYOUT);

        // add new wizzard to wizzard set
        this.wizardList.add(wizard);
    }

    /**
     * When an <tt>AccountRegistrationWizard</tt> has been removed from the
     * <tt>AccountRegistrationWizardContainer</tt> removes the corresponding
     * line from the table.
     */
    public void accountRegistrationRemoved(AccountRegistrationEvent event)
    {
        AccountRegistrationWizard wizard =
            (AccountRegistrationWizard) event.getSource();

        // TODO remove radio button from radio group

        wizardList.remove(wizard);
    }

    /**
     * Implements the <code>WizardPage.getIdentifier</code> method.
     * 
     * @return the page identifier, which in this case is the
     *         DEFAULT_PAGE_IDENTIFIER, which means that this page is the
     *         default one for the wizard.
     */
    public Object getIdentifier()
    {
        return WizardPage.DEFAULT_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getNextPageIdentifier</code> method.
     * 
     * @return the identifier of the next wizard page, which in this case is set
     *         dynamically when user selects a row in the table.
     */
    public Object getNextPageIdentifier()
    {
        if (nextPageIdentifier == null)
        {
            return WizardPage.DEFAULT_PAGE_IDENTIFIER;
        }
        else
        {
            return nextPageIdentifier;
        }
    }

    /**
     * Implements the <code>WizardPage.getBackPageIdentifier</code> method.
     * 
     * @return this identifier of the previous wizard page, which in this case
     *         is null because this is the first page of the wizard.
     */
    public Object getBackPageIdentifier()
    {
        return null;
    }

    /**
     * Implements the <code>WizardPage.getWizardForm</code> method.
     * 
     * @return this panel
     */
    public Object getWizardForm()
    {
        return this;
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    /**
     * Before the panel is displayed checks the selections and enables the next
     * button if a checkbox is already selected or disables it if nothing is
     * selected.
     */
    public void pageShowing()
    {
        if (this.getCheckedRadioButtonId() != -1)
            this.wizardContainer.setNextFinishButtonEnabled(true);
        else
            this.wizardContainer.setNextFinishButtonEnabled(false);

        if (wizardContainer.getCurrentWizard() != null)
            this.wizardContainer.unregisterWizardPages();
    }

    /**
     * Implements the <tt>WizardPage.pageNext</tt> method, which is invoked
     * from the wizard container when user clicks the "Next" button. We set here
     * the next page identifier to the identifier of the first page of the
     * choosen wizard and register all the pages contained in this wizard in our
     * wizard container.
     */
    public void pageNext()
    {
        int selectedRowIndex = this.getCheckedRadioButtonId();

        AccountRegistrationWizard wizard =
            (AccountRegistrationWizard) wizardList.elementAt(selectedRowIndex);

        Iterator i = wizard.getPages();

        while (i.hasNext())
        {
            WizardPage page = (WizardPage) i.next();

            nextPageIdentifier = page.getIdentifier();

            break;
        }

        wizard.setModification(false);

        this.wizardContainer.setCurrentWizard(wizard);
    }

    public void pageBack()
    {
    }
}
