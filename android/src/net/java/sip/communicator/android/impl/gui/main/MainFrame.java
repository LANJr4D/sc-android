/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.main;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.GuiActivator;
import net.java.sip.communicator.android.impl.gui.main.chat.ChatWindowManager;
import net.java.sip.communicator.android.impl.gui.main.contactlist.ContactListPanel;
import net.java.sip.communicator.android.impl.gui.main.login.LoginManager;
import net.java.sip.communicator.service.configuration.ConfigurationService;
import net.java.sip.communicator.service.configuration.event.PropertyChangeEvent;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.service.protocol.event.*;
import net.java.sip.communicator.util.Logger;
import android.content.Context;
import android.graphics.Color;
import android.view.UIThreadUtilities;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost.TabContentFactory;

/**
 * The main application window. This class is the core of this ui
 * implementation. It stores all available protocol providers and their
 * operation sets, as well as all registered accounts, the
 * <tt>MetaContactListService</tt> and all sent messages that aren't delivered
 * yet.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class MainFrame
{
    private Logger logger = Logger.getLogger(MainFrame.class.getName());

    private LinkedHashMap protocolProviders = new LinkedHashMap();

    private MetaContactListService contactList;

    private LoginManager loginManager;

    private Context androidContext;

    /**
     * Container view for contact list/ chat views.
     */
    private LinearLayout contentView;

    /**
     * Parent container view for main frame.
     */
    private LinearLayout rootView;

    private ContactListPanel contactListPanel;

    /**
     * Button displaying contact list view.
     */
    private Button contactListButton;

    /**
     * Button displaying chat view.
     */
    private Button chatButton;

    private ChatWindowManager chatWindowManager;

    private LinearLayout buttonLayout;

    /**
     * Flag indicating whether contact list view is being displayed.
     */
    boolean contactListDisplayed = false;

    Object synchChatWindowManager = new Object();

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>MainFrame</tt>.
     */
    public MainFrame(Context context)
    {
        this.androidContext = context;

        rootView = new LinearLayout(context);
        rootView.setOrientation(LinearLayout.VERTICAL);

        contactListPanel = new ContactListPanel(this);

        buttonLayout = new LinearLayout(context);

        buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
        contactListButton = new Button(androidContext);
        contactListButton.setText("Contacts");
        contactListButton.setTextColor(Color.BLACK);
        contactListButton.setBackgroundColor(Color.LTGRAY);
        contactListButton.setOnClickListener(new OnClickListener()
        {

            public void onClick(View arg0)
            {
                displayContactList();
            }

        });

        chatButton = new Button(androidContext);
        chatButton.setText("Chat");
        chatButton.setBackgroundColor(Color.GRAY);
        chatButton.setOnClickListener(new OnClickListener()
        {

            public void onClick(View arg0)
            {
                displayChatWindowManager();
            }

        });

        buttonLayout.addView(contactListButton, WRAP_CONTENT);
        buttonLayout.addView(chatButton, WRAP_CONTENT);

        contentView = new LinearLayout(context);
        contentView.setOrientation(LinearLayout.VERTICAL);

        contentView.addView(contactListPanel, WRAP_CONTENT);

        rootView.addView(buttonLayout, WRAP_CONTENT);
        rootView.addView(contentView, WRAP_CONTENT);

        contactListDisplayed = true;
    }

    public void displayContactList()
    {
        if (!contactListDisplayed)
        {
            contactListDisplayed = true;

            UIThreadUtilities.runOnUIThread(rootView, new Runnable()
            {

                public void run()
                {
                    // update buttons appearance
                    setTabButton(contactListButton, true);
                    setTabButton(chatButton, false);

                    chatWindowManager.setVisibility(View.GONE);
                    contactListPanel.setVisibility(View.VISIBLE);

                    buttonLayout.invalidate();
                    contentView.invalidate();
                }
            });
        }
    }

    /**
     * Changes button appearence in order to simulate bring to front/send to
     * back operation for contact list and chat tabs.
     * 
     * @param on
     */
    protected void setTabButton(Button tabButton, boolean on)
    {
        if (on)
        {
            tabButton.setTextColor(Color.BLACK);
            tabButton.setBackgroundColor(Color.LTGRAY);
        }
        else
        {
            tabButton.setTextColor(Color.BLACK);
            tabButton.setBackgroundColor(Color.GRAY);
        }
    }

    public void displayChatWindowManager()
    {
        synchronized (synchChatWindowManager)
        {
            if (contactListDisplayed)
            {
                contactListDisplayed = false;

                UIThreadUtilities.runOnUIThread(rootView, new Runnable()
                {

                    public void run()
                    {
                        if (chatWindowManager == null)
                        {
                            chatWindowManager =
                                new ChatWindowManager(MainFrame.this);
                            contentView
                                .addView(chatWindowManager, WRAP_CONTENT);
                        }
                        else
                        {
                            chatWindowManager.setVisibility(View.VISIBLE);
                        }

                        // update buttons appearance
                        setTabButton(contactListButton, false);
                        setTabButton(chatButton, true);

                        contactListPanel.setVisibility(View.GONE);

                        buttonLayout.invalidate();
                        contentView.invalidate();

                    }
                });
            }
        }
    }

    /**
     * Returns the <tt>MetaContactListService</tt>.
     * 
     * @return <tt>MetaContactListService</tt> The current meta contact list.
     */
    public MetaContactListService getContactList()
    {
        return this.contactList;
    }

    /**
     * Initializes the contact list panel.
     * 
     * @param contactList The <tt>MetaContactListService</tt> containing the
     *            contact list data.
     */
    public void setContactList(MetaContactListService contactList)
    {
        this.contactList = contactList;

        contactListPanel.initList(contactList);

        // TODO look at the original version of the method
    }

    /**
     * Adds all protocol supported operation sets.
     * 
     * @param protocolProvider The protocol provider.
     */
    public void addProtocolSupportedOperationSets(
        ProtocolProviderService protocolProvider)
    {
        Map supportedOperationSets =
            protocolProvider.getSupportedOperationSets();

        String ppOpSetClassName =
            OperationSetPersistentPresence.class.getName();
        String pOpSetClassName = OperationSetPresence.class.getName();

        // Obtain the presence operation set.
        if (supportedOperationSets.containsKey(ppOpSetClassName)
            || supportedOperationSets.containsKey(pOpSetClassName))
        {

            OperationSetPresence presence =
                (OperationSetPresence) supportedOperationSets
                    .get(ppOpSetClassName);

            if (presence == null)
            {
                presence =
                    (OperationSetPresence) supportedOperationSets
                        .get(pOpSetClassName);
            }

            presence
                .addProviderPresenceStatusListener(new GUIProviderPresenceStatusListener());
            presence
                .addContactPresenceStatusListener(new GUIContactPresenceStatusListener());
        }

        // Obtain the basic instant messaging operation set.
        String imOpSetClassName =
            OperationSetBasicInstantMessaging.class.getName();

        if (supportedOperationSets.containsKey(imOpSetClassName))
        {

            OperationSetBasicInstantMessaging im =
                (OperationSetBasicInstantMessaging) supportedOperationSets
                    .get(imOpSetClassName);

            // Add to all instant messaging operation sets the Message
            // listener implemented in the ContactListPanel, which handles
            // all received messages.
            im.addMessageListener(getContactListPanel());
        }

        // Obtain the typing notifications operation set.
        String tnOpSetClassName =
            OperationSetTypingNotifications.class.getName();

        if (supportedOperationSets.containsKey(tnOpSetClassName))
        {

            OperationSetTypingNotifications tn =
                (OperationSetTypingNotifications) supportedOperationSets
                    .get(tnOpSetClassName);

            // Add to all typing notification operation sets the Message
            // listener implemented in the ContactListPanel, which handles
            // all received messages.
            tn.addTypingNotificationsListener(this.getContactListPanel());
        }

        // Obtain the basic instant messaging operation set.
        String smsOpSetClassName = OperationSetSmsMessaging.class.getName();

        if (supportedOperationSets.containsKey(smsOpSetClassName))
        {
            OperationSetSmsMessaging smsOpSet =
                (OperationSetSmsMessaging) supportedOperationSets
                    .get(smsOpSetClassName);

            // Add to all instant messaging operation sets the Message
            // listener implemented in the ContactListPanel, which handles
            // all received messages.
            smsOpSet.addMessageListener(getContactListPanel());
        }
    }

    /**
     * Returns a set of all protocol providers.
     * 
     * @return a set of all protocol providers.
     */
    public Iterator getProtocolProviders()
    {
        return ((LinkedHashMap) protocolProviders.clone()).keySet().iterator();
    }

    /**
     * Returns the protocol provider associated to the account given by the
     * account user identifier.
     * 
     * @param accountName The account user identifier.
     * @return The protocol provider associated to the given account.
     */
    public ProtocolProviderService getProtocolProviderForAccount(
        String accountName)
    {
        Iterator i = this.protocolProviders.keySet().iterator();
        while (i.hasNext())
        {
            ProtocolProviderService pps = (ProtocolProviderService) i.next();

            if (pps.getAccountID().getUserID().equals(accountName))
            {
                return pps;
            }
        }

        return null;
    }

    /**
     * Adds a protocol provider.
     * 
     * @param protocolProvider The protocol provider to add.
     */
    public void addProtocolProvider(ProtocolProviderService protocolProvider)
    {
        logger.trace("Add the following protocol provider to the gui: "
            + protocolProvider.getAccountID().getAccountAddress());

        this.protocolProviders.put(protocolProvider, new Integer(
            initiateProviderIndex(protocolProvider)));

        this.addProtocolSupportedOperationSets(protocolProvider);

        // this.addProviderContactHandler(protocolProvider, contactHandler);
    }

    /**
     * Returns the index of the given protocol provider.
     * 
     * @param protocolProvider the protocol provider to search for
     * @return the index of the given protocol provider
     */
    public int getProviderIndex(ProtocolProviderService protocolProvider)
    {
        Object o = protocolProviders.get(protocolProvider);

        if (o != null)
        {
            return ((Integer) o).intValue();
        }
        return 0;
    }

    /**
     * Adds an account to the application.
     * 
     * @param protocolProvider The protocol provider of the account.
     */
    public void removeProtocolProvider(ProtocolProviderService protocolProvider)
    {
        this.protocolProviders.remove(protocolProvider);
        this.updateProvidersIndexes(protocolProvider);

    }

    /**
     * Activates an account. Here we start the connecting process.
     * 
     * @param protocolProvider The protocol provider of this account.
     */
    public void activateAccount(ProtocolProviderService protocolProvider)
    {
        // TODO
    }

    /**
     * Returns the account user id for the given protocol provider.
     * 
     * @return The account user id for the given protocol provider.
     */
    public String getAccount(ProtocolProviderService protocolProvider)
    {
        return protocolProvider.getAccountID().getUserID();
    }

    /**
     * Returns the presence operation set for the given protocol provider.
     * 
     * @param protocolProvider The protocol provider for which the presence
     *            operation set is searched.
     * @return the presence operation set for the given protocol provider.
     */
    public OperationSetPresence getProtocolPresenceOpSet(
        ProtocolProviderService protocolProvider)
    {
        OperationSet opSet =
            protocolProvider.getOperationSet(OperationSetPresence.class);

        if (opSet != null && opSet instanceof OperationSetPresence)
            return (OperationSetPresence) opSet;

        return null;
    }

    /**
     * Returns the Web Contact Info operation set for the given protocol
     * provider.
     * 
     * @param protocolProvider The protocol provider for which the TN is
     *            searched.
     * @return OperationSetWebContactInfo The Web Contact Info operation set for
     *         the given protocol provider.
     */
    public OperationSetWebContactInfo getWebContactInfoOpSet(
        ProtocolProviderService protocolProvider)
    {
        OperationSet opSet =
            protocolProvider.getOperationSet(OperationSetWebContactInfo.class);

        if (opSet != null && opSet instanceof OperationSetWebContactInfo)
            return (OperationSetWebContactInfo) opSet;

        return null;
    }

    /**
     * Returns the telephony operation set for the given protocol provider.
     * 
     * @param protocolProvider The protocol provider for which the telephony
     *            operation set is about.
     * @return OperationSetBasicTelephony The telephony operation set for the
     *         given protocol provider.
     */
    public OperationSetBasicTelephony getTelephonyOpSet(
        ProtocolProviderService protocolProvider)
    {
        OperationSet opSet =
            protocolProvider.getOperationSet(OperationSetBasicTelephony.class);

        if (opSet != null && opSet instanceof OperationSetBasicTelephony)
            return (OperationSetBasicTelephony) opSet;

        return null;
    }

    /**
     * Returns the multi user chat operation set for the given protocol
     * provider.
     * 
     * @param protocolProvider The protocol provider for which the multi user
     *            chat operation set is about.
     * @return OperationSetMultiUserChat The telephony operation set for the
     *         given protocol provider.
     */
    public OperationSetMultiUserChat getMultiUserChatOpSet(
        ProtocolProviderService protocolProvider)
    {
        OperationSet opSet =
            protocolProvider.getOperationSet(OperationSetMultiUserChat.class);

        if (opSet != null && opSet instanceof OperationSetMultiUserChat)
            return (OperationSetMultiUserChat) opSet;

        return null;
    }

    /**
     * Listens for all contactPresenceStatusChanged events in order to refresh
     * the contact list, when a status is changed.
     */
    private class GUIContactPresenceStatusListener
        implements ContactPresenceStatusListener
    {
        /**
         * Indicates that a contact has changed its status.
         * 
         * @param evt the presence event containing information about the
         *            contact status change
         */
        public void contactPresenceStatusChanged(
            ContactPresenceStatusChangeEvent evt)
        {
            ContactListPanel clistPanel = null;
            // TODO : init clistPanel

            Contact sourceContact = evt.getSourceContact();

            MetaContact metaContact =
                contactList.findMetaContactByContact(sourceContact);

            if (metaContact != null
                && (evt.getOldStatus() != evt.getNewStatus()))
            {
                // TODO Update the status in the contact list.
                // clistPanel.getContactList().refreshContact(metaContact);

                // TODO Update the status in chat window.
            }
        }
    }

    /**
     * Listens for all providerStatusChanged and providerStatusMessageChanged
     * events in order to refresh the account status panel, when a status is
     * changed.
     */
    private class GUIProviderPresenceStatusListener
        implements ProviderPresenceStatusListener
    {
        public void providerStatusChanged(ProviderPresenceStatusChangeEvent evt)
        {
            ProtocolProviderService pps = evt.getProvider();

        }

        public void providerStatusMessageChanged(PropertyChangeEvent evt)
        {

        }
    }

    /**
     * Returns the list of all groups.
     * 
     * @return The list of all groups.
     */
    public Iterator getAllGroups()
    {
        return getContactListPanel().getContactList().getAllGroups();
    }

    /**
     * Returns the Meta Contact Group corresponding to the given MetaUID.
     * 
     * @param metaUID An identifier of a group.
     * @return The Meta Contact Group corresponding to the given MetaUID.
     */
    public MetaContactGroup getGroupByID(String metaUID)
    {
        return getContactListPanel().getContactList().getGroupByID(metaUID);
    }

    /**
     * Returns the class that manages user login.
     * 
     * @return the class that manages user login.
     */
    public LoginManager getLoginManager()
    {
        return loginManager;
    }

    /**
     * Sets the class that manages user login.
     * 
     * @param loginManager The user login manager.
     */
    public void setLoginManager(LoginManager loginManager)
    {
        this.loginManager = loginManager;
    }

    /**
     * Returns the panel containing the ContactList.
     * 
     * @return ContactListPanel the panel containing the ContactList
     */
    public ContactListPanel getContactListPanel()
    {
        return contactListPanel;
    }

    /**
     * Checks in the configuration xml if there is already stored index for this
     * provider and if yes, returns it, otherwise creates a new account index
     * and stores it.
     * 
     * @param protocolProvider the protocol provider
     * @return the protocol provider index
     */
    private int initiateProviderIndex(ProtocolProviderService protocolProvider)
    {
        ConfigurationService configService =
            GuiActivator.getConfigurationService();

        String prefix = "net.java.sip.communicator.impl.gui.accounts";

        List accounts = configService.getPropertyNamesByPrefix(prefix, true);

        Iterator accountsIter = accounts.iterator();

        boolean savedAccount = false;

        while (accountsIter.hasNext())
        {
            String accountRootPropName = (String) accountsIter.next();

            String accountUID = configService.getString(accountRootPropName);

            if (accountUID.equals(protocolProvider.getAccountID()
                .getAccountUniqueID()))
            {

                savedAccount = true;
                String index =
                    configService.getString(accountRootPropName
                        + ".accountIndex");

                if (index != null)
                {
                    // if we have found the accountIndex for this protocol
                    // provider
                    // return this index
                    return new Integer(index).intValue();
                }
                else
                {
                    // if there's no stored accountIndex for this protocol
                    // provider, calculate the index, set it in the
                    // configuration
                    // service and return it.

                    int accountIndex =
                        createAccountIndex(protocolProvider,
                            accountRootPropName);
                    return accountIndex;
                }
            }
        }

        if (!savedAccount)
        {
            String accNodeName =
                "acc" + Long.toString(System.currentTimeMillis());

            String accountPackage =
                "net.java.sip.communicator.impl.gui.accounts." + accNodeName;

            configService.setProperty(accountPackage, protocolProvider
                .getAccountID().getAccountUniqueID());

            int accountIndex =
                createAccountIndex(protocolProvider, accountPackage);

            return accountIndex;
        }
        return -1;
    }

    /**
     * Creates and calculates the account index for the given protocol provider.
     * 
     * @param protocolProvider the protocol provider
     * @param accountRootPropName the path to where the index should be saved in
     *            the configuration xml
     * @return the created index
     */
    private int createAccountIndex(ProtocolProviderService protocolProvider,
        String accountRootPropName)
    {
        ConfigurationService configService =
            GuiActivator.getConfigurationService();

        int accountIndex = -1;
        Iterator pproviders = protocolProviders.keySet().iterator();
        ProtocolProviderService pps;

        while (pproviders.hasNext())
        {
            pps = (ProtocolProviderService) pproviders.next();

            if (pps.getProtocolDisplayName().equals(
                protocolProvider.getProtocolDisplayName())
                && !pps.equals(protocolProvider))
            {

                int index = ((Integer) protocolProviders.get(pps)).intValue();

                if (accountIndex < index)
                {
                    accountIndex = index;
                }
            }
        }
        accountIndex++;
        configService.setProperty(accountRootPropName + ".accountIndex",
            new Integer(accountIndex));

        return accountIndex;
    }

    /**
     * Updates the indexes in the configuration xml, when a provider has been
     * removed.
     * 
     * @param removedProvider the removed protocol provider
     */
    private void updateProvidersIndexes(ProtocolProviderService removedProvider)
    {
        ConfigurationService configService =
            GuiActivator.getConfigurationService();

        String prefix = "net.java.sip.communicator.impl.gui.accounts";

        Iterator pproviders = protocolProviders.keySet().iterator();
        ProtocolProviderService currentProvider = null;
        int sameProtocolProvidersCount = 0;

        while (pproviders.hasNext())
        {
            ProtocolProviderService pps =
                (ProtocolProviderService) pproviders.next();

            if (pps.getProtocolDisplayName().equals(
                removedProvider.getProtocolDisplayName()))
            {

                sameProtocolProvidersCount++;
                if (sameProtocolProvidersCount > 1)
                {
                    break;
                }
                currentProvider = pps;
            }
        }

        if (sameProtocolProvidersCount < 2 && currentProvider != null)
        {
            protocolProviders.put(currentProvider, new Integer(0));

            List accounts =
                configService.getPropertyNamesByPrefix(prefix, true);

            Iterator accountsIter = accounts.iterator();

            while (accountsIter.hasNext())
            {
                String rootPropName = (String) accountsIter.next();

                String accountUID = configService.getString(rootPropName);

                if (accountUID.equals(currentProvider.getAccountID()
                    .getAccountUniqueID()))
                {

                    configService.setProperty(rootPropName + ".accountIndex",
                        new Integer(0));
                }
            }

            // TODO update status in status panel
        }
    }

    /**
     * If the protocol provider supports presence operation set searches the
     * last status which was selected, otherwise returns null.
     * 
     * @param protocolProvider the protocol provider we're interested in.
     * @return the last protocol provider presence status, or null if this
     *         provider doesn't support presence operation set
     */
    public Object getProtocolProviderLastStatus(
        ProtocolProviderService protocolProvider)
    {
        // TODO getLastPresenceStatus(protocolProvider);
        return null;
    }

    public View getView()
    {
        return rootView;
    }

    public Context getAndroidContext()
    {
        return androidContext;
    }

    class ContactListTabFactory
        implements TabContentFactory
    {

        public View createTabContent(String s)
        {
            return contactListPanel;
        }

    }

    public ChatWindowManager getChatWindowManager()
    {
        if (chatWindowManager == null)
        {
            // wait a little bit, chatWindowManager may intialize in a UI Thread
            // right now
            try
            {
                Thread.currentThread().sleep(1000);
            }
            catch (InterruptedException e)
            {
                logger.error(e);
            }
        }
        return chatWindowManager;
    }

}