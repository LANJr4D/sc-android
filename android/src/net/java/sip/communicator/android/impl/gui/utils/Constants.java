/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */

package net.java.sip.communicator.android.impl.gui.utils;

import net.java.sip.communicator.util.*;

/**
 * All look and feel related constants are stored here.
 *
 * @author Yana Stamcheva
 */

public class Constants
{
    private static Logger logger = Logger.getLogger(Constants.class);

    /**
     * Indicates that the user is connected and ready to communicate.
     */
    public static final String ONLINE_STATUS = "Online";

    /**
     * Indicates that the user is disconnected.
     */
    public static final String OFFLINE_STATUS = "Offline";

    /**
     * Indicates that the user is away.
     */
    public static final String AWAY_STATUS = "Away";

    /**
     * Indicates that the user is connected and eager to communicate.
     */
    public static final String FREE_FOR_CHAT_STATUS = "FreeForChat";

    /**
     * Indicates that the user is connected and eager to communicate.
     */
    public static final String DO_NOT_DISTURB_STATUS = "DoNotDisturb";

    /*
     * ===================================================================
     * ------------------------ CONFIG CONSTANTS ---------------------------
     * ===================================================================
     */
    /**
     * Indicates whether to ask for confirmation when user tries to delete
     * a contact.
     */
    public static boolean REMOVE_CONTACT_ASK = true;

    /**
     * Indicates whether to ask for confirmation when user tries to delete
     * a contact.
     */
    public static boolean REMOVE_GROUP_ASK = true;

    /**
     * Indicates the number of messages to show in the chat area when a new
     * chat is opened.
     */
    public static int CHAT_HISTORY_SIZE = 10;

    /*
     * ===================================================================
     * ------------------------ MESSAGE TYPES ---------------------------
     * ===================================================================
     */

    /**
     * The message type representing outgoing messages.
     */
    public static final String OUTGOING_MESSAGE = "OutgoingMessage";

    /**
     * The message type representing incoming messages.
     */
    public static final String INCOMING_MESSAGE = "IncomingMessage";

    /**
     * The message type representing status messages.
     */
    public static final String STATUS_MESSAGE = "StatusMessage";

    /**
     * The message type representing action messages. These are message specific
     * for IRC, but could be used in other protocols also.
     */
    public static final String ACTION_MESSAGE = "ActionMessage";

    /**
     * The message type representing system messages.
     */
    public static final String SYSTEM_MESSAGE = "SystemMessage";

    /**
     * The message type representing sms messages.
     */
    public static final String SMS_MESSAGE = "SmsMessage";

    /**
     * The message type representing error messages.
     */
    public static final String ERROR_MESSAGE = "ErrorMessage";

    /**
     * The history incoming message type.
     */
    public static final String HISTORY_INCOMING_MESSAGE = "HistoryIncomingMessage";

    /**
     * The history outgoing message type.
     */
    public static final String HISTORY_OUTGOING_MESSAGE = "HistoryOutgoingMessage";

    /**
     * The size of the buffer that indicates how many messages will be stored
     * in the conversation area in the chat window.
     */
    public static final int CHAT_BUFFER_SIZE = 3000;

    /*
     * ======================================================================
     * -------------------- FONTS AND COLOR CONSTANTS ------------------------
     * ======================================================================
     */

    /*
     * ======================================================================
     * ------------------------ PROTOCOL NAMES -------------------------------
     * ======================================================================
     */
    /**
     * The ICQ protocol.
     */
    public static final String ICQ = "ICQ";

    /**
     * The MSN protocol.
     */
    public static final String MSN = "MSN";

    /**
     * The AIM protocol.
     */
    public static final String AIM = "AIM";

    /**
     * The Yahoo protocol.
     */
    public static final String YAHOO = "Yahoo";

    /**
     * The Jabber protocol.
     */
    public static final String JABBER = "Jabber";

    /**
     * The Skype protocol.
     */
    public static final String SKYPE = "Skype";

    /**
     * The Gibberish protocol.
     */
    public static final String GIBBERISH = "Gibberish";


    /**
     * The SIP protocol.
     */
    public static final String SIP = "SIP";

    /*
     * ======================================================================
     * ------------------------ OTHER CONSTANTS ------------------------------
     * ======================================================================
     */
    /**
     * Indicates whether the application is in mode "group messages in one
     * window".
     */
    public static final boolean TABBED_CHAT_WINDOW = true;

    /**
     * The default path, where chat window styles could be found.
     */
    public static final String DEFAULT_STYLE_PATH
        = "resources/styles";

    /*
     * ======================================================================
     * ------------------------ SPECIAL CHARS LIST --------------------------
     * ======================================================================
     */
}
