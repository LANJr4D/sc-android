/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.account;

import java.util.*;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.android.service.gui.event.*;
import net.java.sip.communicator.service.protocol.*;
import android.content.*;
import android.widget.*;

/**
 * The <tt>AccountRegSummaryPage</tt> is the last page of the account
 * registration wizard.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AccountRegSummaryPage
    extends TableLayout
    implements WizardPage
{
    private String backPageIdentifier;

    private AccountRegWizardContainerImpl wizardContainer;

    private static final TableRow.LayoutParams ROW_WRAP_CONTENT_LAYOUT =
        new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private static final TableLayout.LayoutParams TABLE_WRAP_CONTENT_LAYOUT =
        new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an <tt>AccountRegSummaryPage</tt>.
     * 
     * @param wizardContainer The account registration wizard container where
     *            this summary page is registered.
     */
    public AccountRegSummaryPage(AccountRegWizardContainerImpl wizardContainer,
        Context androidContext)
    {
        super(androidContext);

        this.wizardContainer = wizardContainer;
    }

    /**
     * Initializes the summary with the data.
     * 
     * @param summaryData The data to insert in the summary page.
     */
    private void init(Iterator summaryData)
    {
        while (summaryData.hasNext())
        {
            Map.Entry entry = (Map.Entry) summaryData.next();

            TableRow tableRow = new TableRow(this.mContext);

            TextView keyLabel = new TextView(this.mContext);
            keyLabel.setText(entry.getKey().toString() + ":");
            tableRow.addView(keyLabel, ROW_WRAP_CONTENT_LAYOUT);

            TextView valueLabel = new TextView(this.mContext);
            valueLabel.setText(entry.getValue().toString());
            tableRow.addView(valueLabel, ROW_WRAP_CONTENT_LAYOUT);

            this.addView(tableRow, TABLE_WRAP_CONTENT_LAYOUT);
        }
    }

    /**
     * Implements the <code>WizardPage.getIdentifier</code> method.
     * 
     * @return the page identifier, which in this case is the
     *         SUMMARY_PAGE_IDENTIFIER
     */
    public Object getIdentifier()
    {
        return WizardPage.SUMMARY_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getNextPageIdentifier</code> method.
     * 
     * @return the FINISH_PAGE_IDENTIFIER to indicate that this is the last
     *         wizard page
     */
    public Object getNextPageIdentifier()
    {
        return WizardPage.FINISH_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getBackPageIdentifier</code> method.
     * 
     * @return the previous page
     */
    public Object getBackPageIdentifier()
    {
        return wizardContainer.getCurrentWizard().getLastPageIdentifier();
    }

    /**
     * Implements the <code>WizardPage.getWizardForm</code> method.
     * 
     * @return this panel
     */
    public Object getWizardForm()
    {
        return this;
    }

    /**
     * Before the panel is displayed obtains the summary data from the current
     * wizard.
     */
    public void pageShowing()
    {
        AccountRegistrationWizard wizard =
            this.wizardContainer.getCurrentWizard();

        this.removeAllViews();

        this.init(wizard.getSummary());
    }

    /**
     * Implements the <tt>WizardPage.pageNext</tt> method, which is invoked
     * from the wizard container when user clicks the "Next" button. We invoke
     * here the wizard finish method.
     */
    public void pageNext()
    {
        AccountRegistrationWizard wizard =
            this.wizardContainer.getCurrentWizard();

        ProtocolProviderService protocolProvider = wizard.finish();

        if (protocolProvider != null)
            this.wizardContainer.saveAccountWizard(protocolProvider, wizard);

        this.wizardContainer.unregisterWizardPages();
    }

    public void pageBack()
    {
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }
}
