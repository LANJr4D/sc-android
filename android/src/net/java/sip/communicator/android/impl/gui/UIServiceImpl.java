/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui;

import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.android.impl.gui.main.account.*;
import net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact.*;
import net.java.sip.communicator.android.impl.gui.main.login.*;
import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.android.service.gui.event.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

/**
 * An implementation of the <tt>UIService</tt> that gives access to other
 * bundles to this particular android ui implementation.
 * 
 * @author Cristina Tabacaru
 */
public class UIServiceImpl
    implements UIService
{
    private static final Logger logger = Logger.getLogger(UIServiceImpl.class);

    private static MainFrame mainFrame;

    private LoginManager loginManager;

    /**
     * View container that will host each screen of the application. Whenever
     * the application switch between screens, the current screen will be
     * diplayed within containerView
     */
    private static LinearLayout containerView;

    /**
     * Root view container of the application, parent of containerView
     */
    private static LinearLayout rootView;

    private static LinearLayout.LayoutParams WRAP_CONTENT_LP =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);

    private static final LinearLayout.LayoutParams FILL_PARENT =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
            LinearLayout.LayoutParams.FILL_PARENT);

    private AccountRegWizardContainerImpl wizardContainer;

    private AddContactWizard addContactWizard;

    private Context androidContext;

    /**
     * Creates an instance of <tt>UIServiceImpl</tt>.
     */
    public UIServiceImpl(Context androidContext)
    {
        this.androidContext = androidContext;

        wizardContainer = new AccountRegWizardContainerImpl(androidContext);

        // initialize root/container view
        rootView = new LinearLayout(androidContext);
        rootView.setPreferredHeight(LinearLayout.LayoutParams.FILL_PARENT);
        rootView.setPreferredWidth(LinearLayout.LayoutParams.FILL_PARENT);

        containerView = new LinearLayout(androidContext);

        TextView logoTextView = new TextView(androidContext);
        logoTextView.setText("SIP Communicator");
        containerView.addView(logoTextView, WRAP_CONTENT_LP);

        rootView.addView(containerView, FILL_PARENT);

        this.mainFrame = new MainFrame(androidContext);

        this.loginManager = new LoginManager(mainFrame);

        mainFrame.setContactList(GuiActivator.getMetaContactListService());
    }

    /**
     * Initializes all views and containers and shows the gui.
     */
    public void loadApplicationGui()
    {

        Thread t = new Thread("GUI launcher")
        {
            public void run()
            {
                Looper.prepare();

                Handler loginLauncherHandler = new Handler();
                loginLauncherHandler.post(new RunApplicationGui());

                Looper.loop();
            }
        };
        t.start();
    }

    /**
     * Returns the LoginManager.
     * 
     * @return the LoginManager
     */
    public LoginManager getLoginManager()
    {
        return loginManager;
    }

    /**
     * The <tt>RunLogin</tt> implements the Runnable interface and is used to
     * shows the login windows in a separate thread.
     */
    private class RunApplicationGui
        implements Runnable
    {
        public void run()
        {
            boolean hasRegisteredAccounts = loginManager.runLogin(mainFrame);
            if (hasRegisteredAccounts)
            {
                displayMainFrame();
            }
        }
    }

    /**
     * Displays main view of the application with the contact list and chat
     * tabs.
     */
    public static void displayMainFrame()
    {
        setCurrentView(mainFrame.getView());
    }

    /**
     * Updates UI with a new view according to the users's actions
     * 
     * @param newCurrentView new current view of the UI
     */
    public static void setCurrentView(final View newCurrentView)
    {

        UIThreadUtilities.runOnUIThread(rootView, new Runnable()
        {
            public void run()
            {
                containerView.removeAllViews();
                containerView.addView(newCurrentView, FILL_PARENT);
                containerView.invalidate();
            }
        });
    }

    /**
     * Gets the view corresponding to a new account registration wizzard.
     * 
     * @return View object
     */
    public View displayNewAccountRegistrationWizard()
    {
        AccountRegWizardContainerImpl wizard = wizardContainer;

        // TODO set wizzard title

        wizard.newAccount();
        wizard.setWizardIsShown(true);

        containerView.removeAllViews();
        containerView.addView(wizardContainer.getWizardView(), WRAP_CONTENT_LP);

        return rootView;
    }

    /**
     * Gets the view corresponding to a new account registration wizzard.
     * 
     * @return View object
     */
    public View displayAddContactWizard()
    {
        if (addContactWizard == null)
        {
            addContactWizard = new AddContactWizard(mainFrame);
        }

        // TODO set wizzard title

        addContactWizard.newContactToAdd();
        addContactWizard.setWizardIsShown(true);

        containerView.removeAllViews();
        containerView
            .addView(addContactWizard.getWizardView(), WRAP_CONTENT_LP);

        return rootView;
    }

    /**
     * Implements the <code>UIService.getAccountRegWizardContainer</code>
     * method. Returns the current implementation of the
     * <tt>AccountRegistrationWizardContainer</tt>.
     * 
     * @see UIService#getAccountRegWizardContainer()
     * 
     * @return a reference to the currently valid instance of
     *         <tt>AccountRegistrationWizardContainer</tt>.
     */
    public AccountRegistrationWizardContainer getAccountRegWizardContainer()
    {
        return this.wizardContainer;
    }

    public Context getAndroidContext()
    {
        return androidContext;
    }

    /**
     * Method returning the root container of the application UI.
     * 
     * @return View object representing
     */
    public View getRootView()
    {
        return rootView;
    }
}
