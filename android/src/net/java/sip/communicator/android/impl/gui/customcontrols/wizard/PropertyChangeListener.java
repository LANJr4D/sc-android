package net.java.sip.communicator.android.impl.gui.customcontrols.wizard;

import java.util.EventListener;

public interface PropertyChangeListener
    extends EventListener
{

    public abstract void propertyChange(PropertyChangeEvent propertychangeevent);
}