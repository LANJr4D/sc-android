/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.customcontrols.wizard.*;
import net.java.sip.communicator.android.impl.gui.main.*;
import net.java.sip.communicator.service.contactlist.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;

/**
 * The <tt>AddContactWizard</tt> is the wizard the guides the user through the
 * process of adding a contact.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class AddContactWizard
    extends Wizard
    implements WizardListener
{
    private Logger logger = Logger.getLogger(AddContactWizard.class.getName());

    private MainFrame mainFrame;

    private NewContact newContact = new NewContact();

    private AddContactWizardPage1 page1;

    private AddContactWizardPage2 page2;

    private AddContactWizardPage3 page3;

    public AddContactWizard(MainFrame mainFrame)
    {
        super(mainFrame.getAndroidContext());

        this.mainFrame = mainFrame;

        super.addWizardListener(this);

        // TODO
        // this.setTitle(Messages.getI18NString("addContactWizard").getText());

        Context androidContext = mainFrame.getAndroidContext();

        page1 =
            new AddContactWizardPage1(this, newContact, mainFrame
                .getProtocolProviders(), androidContext);

        this.registerWizardPage(AddContactWizardPage1.IDENTIFIER, page1);

        page2 =
            new AddContactWizardPage2(this, newContact, mainFrame
                .getAllGroups(), androidContext);

        this.registerWizardPage(AddContactWizardPage2.IDENTIFIER, page2);

        page3 = new AddContactWizardPage3(this, newContact, androidContext);

        this.registerWizardPage(AddContactWizardPage3.IDENTIFIER, page3);

        this.setCurrentPage(AddContactWizardPage1.IDENTIFIER);
    }

    /**
     * Creates a new meta contact in a separate thread.
     */
    private class CreateContact
        extends Thread
    {
        ProtocolProviderService pps;

        MetaContactGroup group;

        NewContact newContact;

        CreateContact(ProtocolProviderService pps, NewContact newContact)
        {
            this.pps = pps;
            this.group = newContact.getGroup();
            this.newContact = newContact;
        }

        public void run()
        {
            try
            {
                mainFrame.getContactList().createMetaContact(pps, group,
                    newContact.getUin());
            }
            catch (MetaContactListException ex)
            {
                logger.error(ex);
                ex.printStackTrace();
                int errorCode = ex.getErrorCode();

                if (errorCode == MetaContactListException.CODE_CONTACT_ALREADY_EXISTS_ERROR)
                {
                    // TODO display eror in GUI
                }
                else if (errorCode == MetaContactListException.CODE_LOCAL_IO_ERROR)
                {
                    // TODO display eror in GUI
                }
                else if (errorCode == MetaContactListException.CODE_NETWORK_ERROR)
                {
                    // TODO display eror in GUI
                }
                else
                {
                    // TODO display eror in GUI
                }
            }
        }
    }

    public void wizardFinished(WizardEvent e)
    {
        if (e.getEventCode() == WizardEvent.SUCCESS)
        {

            ArrayList ppList = newContact.getProtocolProviders();

            for (int i = 0; i < ppList.size(); i++)
            {
                ProtocolProviderService pps =
                    (ProtocolProviderService) ppList.get(i);

                new CreateContact(pps, newContact).start();
            }
        }
    }

    /**
     * Initializes th wizard by setting the select accounts page as the first
     * page to be displayed.
     */
    public void newContactToAdd()
    {
        setCurrentPage(AddContactWizardPage1.IDENTIFIER);
    }

    /**
     * This dialog could not be minimized.
     */
    public void minimize()
    {
    }

    /**
     * This dialog could not be maximized.
     */
    public void maximize()
    {
    }

    /**
     * Returns the main application window.
     * 
     * @return the main application window
     */
    public MainFrame getMainFrame()
    {
        return mainFrame;
    }

    /**
     * The source of the window
     * 
     * @return the source of the window
     */
    public Object getSource()
    {
        return this;
    }
}
