/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.impl.gui.main.contactlist.addcontact;

import java.util.*;

import net.java.sip.communicator.android.impl.gui.*;
import net.java.sip.communicator.android.impl.gui.i18n.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;

import org.osgi.framework.*;

import android.content.*;
import android.view.*;
import android.widget.*;

/**
 * The <tt>SelectAccountPanel</tt> is where the user should select the
 * account, where the new contact will be created.
 * 
 * @author Yana Stamcheva
 * @author Cristina Tabacaru
 */
public class SelectAccountPanel
    extends LinearLayout
    implements ServiceListener
{
    private Logger logger = Logger.getLogger(SelectAccountPanel.class);

    private NewContact newContact;

    private LinearLayout accountListView;

    private int selectedAccounts = 0;
    
    private CompoundButton.OnCheckedChangeListener changeListener;

    private static final LinearLayout.LayoutParams WRAP_CONTENT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates and initializes the <tt>SelectAccountPanel</tt>.
     * 
     * @param newContact An object that collects all user choices through the
     *            wizard.
     * @param protocolProvidersList The list of available
     *            <tt>ProtocolProviderServices</tt>, from which the user
     *            could select.
     */
    public SelectAccountPanel(NewContact newContact,
        Iterator protocolProvidersList, Context androidContext, CompoundButton.OnCheckedChangeListener changeListener)
    {
        super(androidContext);
        this.setOrientation(LinearLayout.VERTICAL);
        
        this.changeListener = changeListener;

        TextView selectProviderTitle = new TextView(androidContext);
        selectProviderTitle.setText(Messages.getI18NString(
            "selectProvidersWizardTitle").getText());
        selectProviderTitle.setPadding(0, 10, 0, 0);
        this.addView(selectProviderTitle, WRAP_CONTENT);

        TextView selectProviderInfo = new TextView(androidContext);
        selectProviderInfo.setText(Messages.getI18NString(
            "selectProvidersWizard").getText());
        selectProviderInfo.setPadding(0, 10, 0, 0);
        this.addView(selectProviderInfo, WRAP_CONTENT);

        this.newContact = newContact;

        this.accountListView = new LinearLayout(androidContext);
        accountListView.setOrientation(LinearLayout.VERTICAL);
        accountListView.setPadding(0, 20, 0, 0);

        this.accountListInit(protocolProvidersList);

        GuiActivator.bundleContext.addServiceListener(this);
    }

    /**
     * Initializes the accounts table.
     */
    private void accountListInit(Iterator protocolProvidersList)
    {
        Context androidContext = this.mContext;

        while (protocolProvidersList.hasNext())
        {
            ProtocolProviderService pps =
                (ProtocolProviderService) protocolProvidersList.next();

            OperationSet opSet =
                pps.getOperationSet(OperationSetPresence.class);

            if (opSet == null)
                continue;

            String userId = pps.getAccountID().getUserID();

            AccountCheckBox accountCheckBox =
                new AccountCheckBox(pps, androidContext);
            accountCheckBox.setText(userId);
            accountCheckBox.setOnCheckedChangeListener(changeListener);

            accountListView.addView(accountCheckBox, WRAP_CONTENT);
        }

        ScrollView accountsScroll = new ScrollView(androidContext);
        accountsScroll.addView(accountListView, WRAP_CONTENT);
        
        this.addView(accountsScroll, WRAP_CONTENT);
    }

    /**
     * Checks whether there is a selected check box in the table.
     * 
     * @return <code>true</code> if any of the check boxes is selected,
     *         <code>false</code> otherwise.
     */
    public boolean isCheckBoxSelected()
    {
        boolean isSelected = false;

        int totalAccounts = accountListView.getChildCount();

        for (int i = 0; i < totalAccounts; i++)
        {
            View innerView = accountListView.getChildAt(i);

            if (innerView instanceof CheckBox)
            {
                if (((CheckBox) innerView).isChecked())
                {
                    isSelected = true;
                }
            }
        }
        return isSelected;
    }

    public void setSelectedAccounts()
    {
        int totalAccounts = accountListView.getChildCount();

        for (int i = 0; i < totalAccounts; i++)
        {
            View innerView = accountListView.getChildAt(i);

            if (innerView instanceof AccountCheckBox)
            {
                if (((CheckBox) innerView).isChecked())
                {
                    newContact
                        .addProtocolProvider((ProtocolProviderService) ((AccountCheckBox) innerView)
                            .getProtocolProviderService());
                }
            }
        }
    }

    public void serviceChanged(ServiceEvent event)
    {
        // if the event is caused by a bundle being stopped, we don't want to
        // know
        if (event.getServiceReference().getBundle().getState() == Bundle.STOPPING)
        {
            return;
        }

        Object sourceService =
            GuiActivator.bundleContext.getService(event.getServiceReference());

        // we don't care if the source service is not a protocol provider
        if (!(sourceService instanceof ProtocolProviderService))
        {
            return;
        }

        ProtocolProviderService sourcePProvider =
            (ProtocolProviderService) sourceService;

        if (event.getType() == ServiceEvent.REGISTERED)
        {

            String userId = sourcePProvider.getAccountID().getUserID();

            AccountCheckBox accountCheckBox =
                new AccountCheckBox(sourcePProvider, this.mContext);
            accountCheckBox.setText(userId);

            accountListView.addView(accountCheckBox, WRAP_CONTENT);

            // TODO refresh GUI
        }
        else if (event.getType() == ServiceEvent.UNREGISTERING)
        {

            int totalAccounts = accountListView.getChildCount();

            for (int i = 0; i < totalAccounts; i++)
            {
                View innerView = accountListView.getChildAt(i);

                if (innerView instanceof AccountCheckBox)
                {
                    if (((AccountCheckBox) innerView)
                        .getProtocolProviderService().equals(sourcePProvider))
                    {
                        accountListView.removeView(innerView);
                        break;
                    }
                }
            }

            // TODO refresh GUI
        }
    }

}
