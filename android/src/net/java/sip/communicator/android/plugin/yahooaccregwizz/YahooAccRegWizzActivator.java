/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.plugin.yahooaccregwizz;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.android.service.gui.event.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;

import org.osgi.framework.*;

/**
 * Registers the <tt>YahooAccountRegistrationWizard</tt> in the UI Service.
 * 
 * @author Yana Stamcheva
 */
public class YahooAccRegWizzActivator
    implements BundleActivator
{

    public static BundleContext bundleContext;

    private static Logger logger =
        Logger.getLogger(YahooAccRegWizzActivator.class.getName());

    private static AccountRegistrationWizardContainer wizardContainer;

    private static YahooAccountRegistrationWizard yahooWizard;

    private static UIService uiService;

    /**
     * Starts this bundle.
     * 
     * @param bc BundleContext
     * @throws Exception
     */
    public void start(BundleContext bc) throws Exception
    {
        try
        {
            bundleContext = bc;

            ServiceReference uiServiceRef =
                bundleContext.getServiceReference(UIService.class.getName());

            uiService = (UIService) bundleContext.getService(uiServiceRef);

            wizardContainer =
                (AccountRegistrationWizardContainer) uiService
                    .getAccountRegWizardContainer();

            yahooWizard =
                new YahooAccountRegistrationWizard(wizardContainer, uiService
                    .getAndroidContext());

            wizardContainer.addAccountRegistrationWizard(yahooWizard);
        }
        catch (Exception e)
        {
            logger.error("Error starting YahooAccRegWizzActivator", e);
        }
    }

    public void stop(BundleContext bundleContext) throws Exception
    {
        wizardContainer.removeAccountRegistrationWizard(yahooWizard);
    }

    /**
     * Returns the <tt>ProtocolProviderFactory</tt> for the Yahoo protocol.
     * 
     * @return the <tt>ProtocolProviderFactory</tt> for the Yahoo protocol
     */
    public static ProtocolProviderFactory getYahooProtocolProviderFactory()
    {

        ServiceReference[] serRefs = null;

        String osgiFilter =
            "(" + ProtocolProviderFactory.PROTOCOL + "=" + ProtocolNames.YAHOO
                + ")";

        try
        {
            serRefs =
                bundleContext.getServiceReferences(
                    ProtocolProviderFactory.class.getName(), osgiFilter);
        }
        catch (InvalidSyntaxException ex)
        {
            logger.error("YahooAccRegWizzActivator : " + ex);
        }

        return (ProtocolProviderFactory) bundleContext.getService(serRefs[0]);
    }

    /**
     * Returns the <tt>UIService</tt>.
     * 
     * @return the <tt>UIService</tt>
     */
    public static UIService getUIService()
    {
        return uiService;
    }
}
