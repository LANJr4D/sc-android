/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.plugin.jabberaccregwizz;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.android.service.gui.event.*;
import net.java.sip.communicator.service.configuration.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;

import org.osgi.framework.*;

/**
 * Registers the <tt>JabberAccountRegistrationWizard</tt> in the UI Service.
 * 
 * @author Yana Stamcheva
 */
public class JabberAccRegWizzActivator
    implements BundleActivator
{

    public static BundleContext bundleContext;

    private static Logger logger =
        Logger.getLogger(JabberAccRegWizzActivator.class.getName());

    private static ConfigurationService configService;

    private static AccountRegistrationWizardContainer wizardContainer;

    private static JabberAccountRegistrationWizard jabberWizard;

    private static UIService uiService;

    /**
     * Starts this bundle.
     * 
     * @param bc BundleContext
     * @throws Exception
     */
    public void start(BundleContext bc) throws Exception
    {

        bundleContext = bc;

        ServiceReference uiServiceRef =
            bundleContext.getServiceReference(UIService.class.getName());

        uiService = (UIService) bundleContext.getService(uiServiceRef);

        wizardContainer =
            (AccountRegistrationWizardContainer) uiService
                .getAccountRegWizardContainer();

        jabberWizard = new JabberAccountRegistrationWizard(wizardContainer, uiService.getAndroidContext());

        wizardContainer.addAccountRegistrationWizard(jabberWizard);
    }

    public void stop(BundleContext bundleContext) throws Exception
    {
        wizardContainer.removeAccountRegistrationWizard(jabberWizard);
    }

    /**
     * Returns the <tt>ProtocolProviderFactory</tt> for the Jabber protocol.
     * 
     * @return the <tt>ProtocolProviderFactory</tt> for the Jabber protocol
     */
    public static ProtocolProviderFactory getJabberProtocolProviderFactory()
    {

        ServiceReference[] serRefs = null;

        String osgiFilter =
            "(" + ProtocolProviderFactory.PROTOCOL + "=" + ProtocolNames.JABBER
                + ")";

        try
        {
            serRefs =
                bundleContext.getServiceReferences(
                    ProtocolProviderFactory.class.getName(), osgiFilter);
        }
        catch (InvalidSyntaxException ex)
        {
            logger.error("JabberAccRegWizzActivator : " + ex);
        }

        return (ProtocolProviderFactory) bundleContext.getService(serRefs[0]);
    }

    /**
     * Returns the <tt>UIService</tt>.
     * 
     * @return the <tt>UIService</tt>
     */
    public static UIService getUIService()
    {
        return uiService;
    }
}
