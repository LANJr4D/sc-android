/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.plugin.jabberaccregwizz;

import java.util.*;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.graphics.*;
import android.text.*;
import android.text.method.*;
import android.widget.*;

/**
 * The <tt>FirstWizardPage</tt> is the page, where user could enter the user
 * ID and the password of the account.
 * 
 * @author Yana Stamcheva
 * @author Damian Minkov
 * @author Cristina Tabacaru
 */
public class FirstWizardPage
    extends TableLayout
    implements WizardPage
{
    private static final Logger logger =
        Logger.getLogger(FirstWizardPage.class);

    public static final String FIRST_PAGE_IDENTIFIER = "FirstPageIdentifier";

    private static final String GOOGLE_USER_SUFFIX = "gmail.com";

    private static final String GOOGLE_CONNECT_SRV = "talk.google.com";

    private static final String DEFAULT_PORT = "5222";

    private static final String DEFAULT_PRIORITY = "10";

    private static final String DEFAULT_RESOURCE = "sip-comm";

    private EditText uinEdit;

    private EditText passEdit;

    private EditText serverEdit;

    private EditText portEdit;

    private EditText resourceEdit;

    private EditText priorityEdit;

    private CheckBox rememberPassButton;

    private CheckBox enableAdvOpButton;

    private TextView existingAccountLabel;

    private TextView invalidInputLabel;

    private Object nextPageIdentifier = WizardPage.SUMMARY_PAGE_IDENTIFIER;

    private JabberAccountRegistrationWizard wizard;

    private static final TableRow.LayoutParams ROW_WRAP_CONTENT_LAYOUT =
        new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private static final TableLayout.LayoutParams TABLE_WRAP_CONTENT_LAYOUT =
        new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private static final LinearLayout.LayoutParams LINEAR_WRAP_CONTENT_LAYOUT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    /**
     * Creates an instance of <tt>FirstWizardPage</tt>.
     * 
     * @param wizard the parent wizard
     */
    public FirstWizardPage(JabberAccountRegistrationWizard wizard,
        Context androidContext)
    {
        super(androidContext);

        logger.info("RO FirstWizardPage currentThread="
            + Thread.currentThread().getName());

        this.wizard = wizard;

        this.init();
    }

    /**
     * Initializes all panels, buttons, etc.
     */
    private void init()
    {
        Context androidContext = this.mContext;

        this.setPreferredHeight(LayoutParams.WRAP_CONTENT);
        this.setPreferredWidth(LayoutParams.FILL_PARENT);

        final TextView idPasswordLabel = new TextView(androidContext);
        idPasswordLabel.setText(Resources.getString("userIDAndPassword"));
        idPasswordLabel.setTextColor(Color.BLACK);
        idPasswordLabel.setBackgroundColor(Color.LTGRAY);

        // uin ROW
        final TableRow uinRow = new TableRow(androidContext);
        LinearLayout editUin = new LinearLayout(androidContext);
        editUin.setOrientation(LinearLayout.VERTICAL);

        final TextView uinText = new TextView(androidContext);
        uinText.setText(Resources.getString("userID"));

        uinEdit = new EditText(androidContext);
        uinEdit.setPreferredWidth(100);
        uinEdit.setTextSize(16f);

        final TextView exUin = new TextView(androidContext);
        exUin.setText("Ex: johnsmith@jabber.org");

        editUin.addView(uinEdit, LINEAR_WRAP_CONTENT_LAYOUT);
        editUin.addView(exUin, LINEAR_WRAP_CONTENT_LAYOUT);

        uinRow.addView(uinText, ROW_WRAP_CONTENT_LAYOUT);
        uinRow.addView(editUin, ROW_WRAP_CONTENT_LAYOUT);

        // pass ROW
        final TableRow passRow = new TableRow(androidContext);

        final TextView passText = new TextView(androidContext);
        passText.setText(Resources.getString("password"));

        passEdit = new EditText(androidContext);
        passEdit.setPreferredWidth(100);
        passEdit.setTextSize(16f);
        passEdit.setTransformationMethod(PasswordTransformationMethod
            .getInstance());

        passRow.addView(passText, ROW_WRAP_CONTENT_LAYOUT);
        passRow.addView(passEdit, ROW_WRAP_CONTENT_LAYOUT);

        // remember pass Check box
        rememberPassButton = new CheckBox(androidContext);
        rememberPassButton.setChecked(true);
        rememberPassButton.setText(Resources.getString("rememberPassword"));

        // final TextView advOptionsLabel = new TextView(androidContext);
        // advOptionsLabel.setText(Resources.getString("advancedOptions"));
        // advOptionsLabel.setTextColor(Color.BLACK);
        // advOptionsLabel.setBackgroundColor(Color.LTGRAY);
        //
        // enableAdvOpButton = new CheckBox(androidContext);
        // enableAdvOpButton.setChecked(false);
        // enableAdvOpButton.setText(Resources.getString("ovverideServerOps"));
        // enableAdvOpButton
        // .setOnCheckedChangeListener(new OnCheckedChangeListener()
        // {
        // public void onCheckedChanged(CompoundButton compoundbutton,
        // boolean flag)
        // {
        // // Perform action
        // CheckBox cb = (CheckBox) compoundbutton;
        //
        // if (!wizard.isModification())
        // serverEdit.setEnabled(cb.isChecked());
        //
        // portEdit.setEnabled(cb.isSelected());
        // resourceEdit.setEnabled(cb.isSelected());
        // priorityEdit.setEnabled(cb.isSelected());
        //
        // if (!cb.isSelected())
        // {
        // setServerFieldAccordingToUserID();
        //
        // portEdit.setText(DEFAULT_PORT);
        // resourceEdit.setText(DEFAULT_RESOURCE);
        // priorityEdit.setText(DEFAULT_PRIORITY);
        // }
        //
        // }
        // });
        //
        // // connect server ROW
        // final TableRow connectServerRow = new TableRow(androidContext);
        //
        // final TextView serverLabel = new TextView(androidContext);
        // serverLabel.setText(Resources.getString("server"));
        //
        // serverEdit = new EditText(androidContext);
        // serverEdit.setEnabled(false);
        //
        // connectServerRow.addView(serverLabel, ROW_WRAP_CONTENT_LAYOUT);
        // connectServerRow.addView(serverEdit, ROW_WRAP_CONTENT_LAYOUT);
        //
        // // port ROW
        // PortAndPriorityTextWatcher textWatcher =
        // new PortAndPriorityTextWatcher();
        //
        // final TableRow portRow = new TableRow(androidContext);
        //
        // final TextView portLabel = new TextView(androidContext);
        // portLabel.setText(Resources.getString("port"));
        //
        // portEdit = new EditText(androidContext);
        // portEdit.setText(DEFAULT_PORT);
        // portEdit.addTextChangedListener(textWatcher);
        // portEdit.setEnabled(false);
        //
        // portRow.addView(portLabel, ROW_WRAP_CONTENT_LAYOUT);
        // portRow.addView(portEdit, ROW_WRAP_CONTENT_LAYOUT);
        //
        // // resource ROW
        // final TableRow resourceRow = new TableRow(androidContext);
        //
        // final TextView resourceLabel = new TextView(androidContext);
        // resourceLabel.setText("Resources");
        //
        // resourceEdit = new EditText(androidContext);
        // resourceEdit.setText(DEFAULT_RESOURCE);
        // resourceEdit.setEnabled(false);
        //
        // resourceRow.addView(resourceLabel, ROW_WRAP_CONTENT_LAYOUT);
        // resourceRow.addView(resourceEdit, ROW_WRAP_CONTENT_LAYOUT);
        //
        // // priority ROW
        // final TableRow priorityRow = new TableRow(androidContext);
        //
        // final TextView priorityLabel = new TextView(androidContext);
        // priorityLabel.setText("Priority");
        //
        // priorityEdit = new EditText(androidContext);
        // priorityEdit.setText(DEFAULT_PRIORITY);
        // priorityEdit.addTextChangedListener(textWatcher);
        // priorityEdit.setEnabled(false);
        //
        // priorityRow.addView(priorityLabel, ROW_WRAP_CONTENT_LAYOUT);
        // priorityRow.addView(priorityEdit, ROW_WRAP_CONTENT_LAYOUT);

        this.addView(idPasswordLabel, TABLE_WRAP_CONTENT_LAYOUT);
        this.addView(uinRow, TABLE_WRAP_CONTENT_LAYOUT);
        this.addView(passRow, TABLE_WRAP_CONTENT_LAYOUT);
        this.addView(rememberPassButton, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(advOptionsLabel, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(enableAdvOpButton, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(connectServerRow, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(portRow, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(resourceRow, TABLE_WRAP_CONTENT_LAYOUT);
        // this.addView(priorityRow, TABLE_WRAP_CONTENT_LAYOUT);

        // init existingAccountLabel for further use
        existingAccountLabel = new TextView(androidContext);
        existingAccountLabel.setText(Resources.getString("existingAccount"));
        existingAccountLabel.setTextColor(Color.RED);

        invalidInputLabel = new TextView(androidContext);
        invalidInputLabel.setText(Resources.getString("invalidInput"));
        invalidInputLabel.setTextColor(Color.RED);
    }

    /**
     * Implements the <code>WizardPage.getIdentifier</code> to return this
     * page identifier.
     * 
     * @return the id of the first wizard page.
     */
    public Object getIdentifier()
    {
        return FIRST_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getNextPageIdentifier</code> to return
     * the next page identifier - the summary page.
     * 
     * @return the id of the next wizard page.
     */
    public Object getNextPageIdentifier()
    {
        return nextPageIdentifier;
    }

    /**
     * Implements the <code>WizardPage.getBackPageIdentifier</code> to return
     * the next back identifier - the default page.
     * 
     * @return the id of the default wizard page.
     */
    public Object getBackPageIdentifier()
    {
        return WizardPage.DEFAULT_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getWizardForm</code> to return this
     * panel.
     * 
     * @return this wizard page.
     */
    public Object getWizardForm()
    {
        return this;
    }

    /**
     * Before this page is displayed enables or disables the "Next" wizard
     * button according to whether the User ID field is empty.
     */
    public void pageShowing()
    {
        this.setNextButtonAccordingToUserIDAndResource();
    }

    /**
     * Saves the user input when the "Next" wizard buttons is clicked.
     */
    public void pageNext()
    {

        String userID = uinEdit.getText().toString();
        String pass = passEdit.getText().toString();

        boolean inValidInput =
            (userID == null) || (userID.trim().equals("")) || (pass == null)
                || (pass.trim().equals(""));

        if (inValidInput)
        {
            // if user credentials are not provided then we remain on the same
            // page and display an error message; we could also use a
            // TextWatcher in order to follow user input, but it slows down user
            // actions on UI
            nextPageIdentifier = FIRST_PAGE_IDENTIFIER;
            if (this.indexOfChild(invalidInputLabel) == -1)
                this.addView(invalidInputLabel, 0, TABLE_WRAP_CONTENT_LAYOUT);
        }
        else
        {
            if (!wizard.isModification() && isExistingAccount(userID))
            {
                nextPageIdentifier = FIRST_PAGE_IDENTIFIER;
                if (this.indexOfChild(existingAccountLabel) == -1)
                    this.addView(existingAccountLabel, 0,
                        TABLE_WRAP_CONTENT_LAYOUT);
            }
            else
            {
                nextPageIdentifier = SUMMARY_PAGE_IDENTIFIER;

                // remove existingAccountLabel from the screen if it has been
                // added
                if (this.indexOfChild(existingAccountLabel) != -1)
                    this.removeView(existingAccountLabel);

                // remove invalidInputLabel from the screen if it has been
                // added
                if (this.indexOfChild(invalidInputLabel) != -1)
                    this.removeView(invalidInputLabel);

                JabberAccountRegistration registration =
                    wizard.getRegistration();

                registration.setUserID(userID);
                registration.setPassword(new String(passEdit.getText()
                    .toString()));
                registration
                    .setRememberPassword(rememberPassButton.isChecked());

                // registration.setServerAddress(serverEdit.getText().toString());
                // registration.setResource(resourceEdit.getText().toString());
                //
                // String portEditText = portEdit.getText().toString();
                //
                // if (portEditText != null)
                // registration.setPort(Integer.parseInt(portEditText));
                //
                // String priorityEditText = priorityEdit.getText().toString();
                //
                // if (priorityEditText != null)
                // registration.setPriority(Integer.parseInt(priorityEditText));
            }
        }
    }

    /**
     * Enables or disables the "Next" wizard button according to whether the
     * UserID field is empty.
     */
    private void setNextButtonAccordingToUserIDAndResource()
    {
        // String uinEditText = uinEdit.getText().toString();
        // // String resourceEditText = resourceEdit.getText().toString();
        //
        // if (uinEditText == null || uinEditText.equals(""))
        // {
        // wizard.getWizardContainer().setNextFinishButtonEnabled(false);
        // }
        // else
        // {
        // wizard.getWizardContainer().setNextFinishButtonEnabled(true);
        // }

        wizard.getWizardContainer().setNextFinishButtonEnabled(true);
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    public void pageBack()
    {
    }

    /**
     * Fills the User ID and Password fields in this panel with the data coming
     * from the given protocolProvider.
     * 
     * @param protocolProvider The <tt>ProtocolProviderService</tt> to load
     *            the data from.
     */
    public void loadAccount(ProtocolProviderService protocolProvider)
    {
        AccountID accountID = protocolProvider.getAccountID();

        Map accountProperties = accountID.getAccountProperties();

        String password =
            (String) accountProperties.get(ProtocolProviderFactory.PASSWORD);

        this.uinEdit.setEnabled(false);
        this.uinEdit.setText(accountID.getUserID());

        if (password != null)
        {
            this.passEdit.setText(password);
            this.rememberPassButton.setChecked(true);
        }

        String serverAddress =
            (String) accountProperties
                .get(ProtocolProviderFactory.SERVER_ADDRESS);

        serverEdit.setText(serverAddress);

        String serverPort =
            (String) accountProperties.get(ProtocolProviderFactory.SERVER_PORT);

        portEdit.setText(serverPort);

        String resource =
            (String) accountProperties.get(ProtocolProviderFactory.RESOURCE);

        resourceEdit.setText(resource);

        String priority =
            (String) accountProperties
                .get(ProtocolProviderFactory.RESOURCE_PRIORITY);

        priorityEdit.setText(priority);

        if (!serverPort.equals(DEFAULT_PORT)
            || !resource.equals(DEFAULT_RESOURCE)
            || !priority.equals(DEFAULT_PRIORITY))
        {
            enableAdvOpButton.setChecked(true);

            // The server field should stay disabled in modification mode,
            // because the user should not be able to change anything concerning
            // the account identifier and server name is part of it.
            serverEdit.setEnabled(false);

            portEdit.setEnabled(true);
            resourceEdit.setEnabled(true);
            priorityEdit.setEnabled(true);
        }
    }

    /**
     * Parse the server part from the jabber id and set it to server as default
     * value. If Advanced option is enabled Do nothing.
     */
    private void setServerFieldAccordingToUserID()
    {

        if (!enableAdvOpButton.isChecked())
        {
            String userID = uinEdit.getText().toString();
            int delimIndex = userID.indexOf("@");
            if (delimIndex != -1)
            {
                String newServerAddr = userID.substring(delimIndex + 1);
                if (newServerAddr.equals(GOOGLE_USER_SUFFIX))
                {
                    serverEdit.setText(GOOGLE_CONNECT_SRV);
                }
                else
                {
                    serverEdit.setText(newServerAddr);
                }
            }
        }
    }

    /**
     * Disables Next Button if Port field value is incorrect
     */
    private void setNextButtonAccordingToPortAndPriority()
    {
        try
        {
            new Integer(portEdit.getText().toString());
            new Integer(priorityEdit.getText().toString());
            wizard.getWizardContainer().setNextFinishButtonEnabled(true);
        }
        catch (NumberFormatException ex)
        {
            wizard.getWizardContainer().setNextFinishButtonEnabled(false);
        }
    }

    /**
     * Checks if the accountName corresponds to an already existing account.
     * 
     * @param accountName the name of the account to check
     * @return TRUE if an account with the specified name already exists, FALSE -
     *         otherwise.
     */
    private boolean isExistingAccount(String accountName)
    {
        ProtocolProviderFactory factory =
            JabberAccRegWizzActivator.getJabberProtocolProviderFactory();

        ArrayList registeredAccounts = factory.getRegisteredAccounts();

        for (int i = 0; i < registeredAccounts.size(); i++)
        {
            AccountID accountID = (AccountID) registeredAccounts.get(i);

            if (accountName.equalsIgnoreCase(accountID.getUserID()))
            {
                return true;
            }
        }
        return false;
    }

    class PortAndPriorityTextWatcher
        implements TextWatcher
    {

        public void beforeTextChanged(CharSequence charsequence, int i, int j,
            int k)
        {
            // TODO Auto-generated method stub

        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
            setNextButtonAccordingToPortAndPriority();
            // TODO setServerFieldAccordingToUserID();
        }

    }
}
