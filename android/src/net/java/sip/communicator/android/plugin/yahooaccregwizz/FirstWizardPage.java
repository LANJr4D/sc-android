/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.plugin.yahooaccregwizz;

import java.util.*;

import net.java.sip.communicator.android.service.gui.*;
import net.java.sip.communicator.service.protocol.*;
import net.java.sip.communicator.util.*;
import android.content.*;
import android.graphics.*;
import android.text.method.*;
import android.widget.*;

/**
 * The <tt>FirstWizardPage</tt> is the page, where user could enter the uin
 * and the password of the account.
 * 
 * @author Yana Stamcheva
 * @author Damian Minkov
 * @author Cristina Tabacaru
 */
public class FirstWizardPage
    extends TableLayout
    implements WizardPage
{

    public static final String FIRST_PAGE_IDENTIFIER = "FirstPageIdentifier";

    private Object nextPageIdentifier = WizardPage.SUMMARY_PAGE_IDENTIFIER;

    private YahooAccountRegistrationWizard wizard;

    private EditText uinEdit;

    private EditText passEdit;

    private CheckBox rememberPass;

    private TextView existingAccountLabel;

    private TextView invalidInputLabel;

    private static final TableRow.LayoutParams ROW_WRAP_CONTENT_LAYOUT =
        new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private static final TableLayout.LayoutParams TABLE_WRAP_CONTENT_LAYOUT =
        new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private static final LinearLayout.LayoutParams LINEAR_WRAP_CONTENT_LAYOUT =
        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT);

    private Logger logger = Logger.getLogger(FirstWizardPage.class);

    /**
     * Creates an instance of <tt>FirstWizardPage</tt>.
     * 
     * @param wizard the parent wizard
     * @param androidContext needed for building UI components
     */
    public FirstWizardPage(YahooAccountRegistrationWizard wizard,
        Context androidContext)
    {

        super(androidContext);

        this.wizard = wizard;

        this.init();
    }

    /**
     * Initializes all panels, buttons, etc.
     */
    private void init()
    {
        Context androidContext = this.mContext;

        this.setPreferredHeight(LayoutParams.WRAP_CONTENT);
        this.setPreferredWidth(LayoutParams.FILL_PARENT);

        // uin ROW
        final TableRow uinRow = new TableRow(androidContext);
        LinearLayout editUin = new LinearLayout(androidContext);
        editUin.setOrientation(LinearLayout.VERTICAL);

        final TextView uinText = new TextView(androidContext);
        uinText.setText(Resources.getString("uin"));

        uinEdit = new EditText(androidContext);
        uinEdit.setPreferredWidth(100);
        uinEdit.setTextSize(16f);

        final TextView exUin = new TextView(androidContext);
        exUin.setText("Ex: johnsmith@yahoo.com or johnsmith");

        editUin.addView(uinEdit, LINEAR_WRAP_CONTENT_LAYOUT);
        editUin.addView(exUin, LINEAR_WRAP_CONTENT_LAYOUT);

        uinRow.addView(uinText, ROW_WRAP_CONTENT_LAYOUT);
        uinRow.addView(editUin, ROW_WRAP_CONTENT_LAYOUT);

        // pass ROW
        final TableRow passRow = new TableRow(androidContext);

        final TextView passText = new TextView(androidContext);
        passText.setText(Resources.getString("password"));

        passEdit = new EditText(androidContext);
        passEdit.setPreferredWidth(100);
        passEdit.setTextSize(16f);
        passEdit.setTransformationMethod(PasswordTransformationMethod
            .getInstance());

        passRow.addView(passText, ROW_WRAP_CONTENT_LAYOUT);
        passRow.addView(passEdit, ROW_WRAP_CONTENT_LAYOUT);

        // remember pass Check box
        rememberPass = new CheckBox(androidContext);
        rememberPass.setChecked(true);
        rememberPass.setText(Resources.getString("rememberPassword"));

        this.addView(uinRow, TABLE_WRAP_CONTENT_LAYOUT);
        this.addView(passRow, TABLE_WRAP_CONTENT_LAYOUT);
        this.addView(rememberPass, TABLE_WRAP_CONTENT_LAYOUT);

        // init existingAccountLabel for further use
        existingAccountLabel = new TextView(this.mContext);
        existingAccountLabel.setText(Resources.getString("existingAccount"));
        existingAccountLabel.setTextColor(Color.RED);

        invalidInputLabel = new TextView(androidContext);
        invalidInputLabel.setText(Resources.getString("invalidInput"));
        invalidInputLabel.setTextColor(Color.RED);
    }

    /**
     * Implements the <code>WizardPage.getIdentifier</code> to return this
     * page identifier.
     */
    public Object getIdentifier()
    {
        return FIRST_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getNextPageIdentifier</code> to return
     * the next page identifier - the summary page.
     */
    public Object getNextPageIdentifier()
    {
        return nextPageIdentifier;
    }

    /**
     * Implements the <code>WizardPage.getBackPageIdentifier</code> to return
     * the next back identifier - the default page.
     */
    public Object getBackPageIdentifier()
    {
        return WizardPage.DEFAULT_PAGE_IDENTIFIER;
    }

    /**
     * Implements the <code>WizardPage.getWizardForm</code> to return this
     * panel.
     */
    public Object getWizardForm()
    {
        return this;
    }

    /**
     * Before this page is displayed enables or disables the "Next" wizard
     * button according to whether the UIN field is empty.
     */
    public void pageShowing()
    {
        this.setNextButtonAccordingToUIN();
    }

    /**
     * Saves the user input when the "Next" wizard buttons is clicked.
     */
    public void pageNext()
    {
        String userID = uinEdit.getText().toString();
        String pass = passEdit.getText().toString();

        boolean inValidInput =
            (userID == null) || (userID.trim().equals("")) || (pass == null)
                || (pass.trim().equals(""));

        if (inValidInput)
        {
            // if user credentials are not provided then we remain on the same
            // page and display an error message; we could also use a
            // TextWatcher in order to follow user input, but it slows down user
            // actions on UI
            nextPageIdentifier = FIRST_PAGE_IDENTIFIER;
            if (this.indexOfChild(invalidInputLabel) == -1)
                this.addView(invalidInputLabel, 0, TABLE_WRAP_CONTENT_LAYOUT);
        }
        else
        {

            if (!wizard.isModification() && isExistingAccount(userID))
            {
                nextPageIdentifier = FIRST_PAGE_IDENTIFIER;

                if (this.indexOfChild(existingAccountLabel) == -1)
                    this.addView(existingAccountLabel, 0,
                        TABLE_WRAP_CONTENT_LAYOUT);
            }
            else
            {
                nextPageIdentifier = SUMMARY_PAGE_IDENTIFIER;

                // remove existingAccountLabel from the screen if it has been
                // added
                if (this.indexOfChild(existingAccountLabel) != -1)
                    this.removeView(existingAccountLabel);

                YahooAccountRegistration registration =
                    wizard.getRegistration();

                registration.setUin(userID);
                registration.setPassword(new String(passEdit.getText()
                    .toString()));
                registration.setRememberPassword(rememberPass.isChecked());
            }
        }
    }

    /**
     * Enables or disables the "Next" wizard button according to whether the UIN
     * field is empty.
     */
    private void setNextButtonAccordingToUIN()
    {
        // String uin = uinEdit.getText().toString();
        //
        // if (uin == null || uin.equals(""))
        // {
        // wizard.getWizardContainer().setNextFinishButtonEnabled(false);
        // }
        // else
        // {
        // wizard.getWizardContainer().setNextFinishButtonEnabled(true);
        // }

        wizard.getWizardContainer().setNextFinishButtonEnabled(true);
    }

    public void pageHiding()
    {
    }

    public void pageShown()
    {
    }

    public void pageBack()
    {
    }

    /**
     * Fills the UIN and Password fields in this panel with the data coming from
     * the given protocolProvider.
     * 
     * @param protocolProvider The <tt>ProtocolProviderService</tt> to load
     *            the data from.
     */
    public void loadAccount(ProtocolProviderService protocolProvider)
    {
        AccountID accountID = protocolProvider.getAccountID();
        String password =
            (String) accountID.getAccountProperties().get(
                ProtocolProviderFactory.PASSWORD);

        this.uinEdit.setEnabled(false);
        this.uinEdit.setText(accountID.getUserID());

        if (password != null)
        {
            this.passEdit.setText(password);
            this.rememberPass.setChecked(true);
        }
    }

    private boolean isExistingAccount(String accountName)
    {
        ProtocolProviderFactory factory =
            YahooAccRegWizzActivator.getYahooProtocolProviderFactory();

        ArrayList registeredAccounts = factory.getRegisteredAccounts();

        for (int i = 0; i < registeredAccounts.size(); i++)
        {
            AccountID accountID = (AccountID) registeredAccounts.get(i);

            if (accountName.equalsIgnoreCase(accountID.getUserID()))
                return true;
        }
        return false;
    }
}
