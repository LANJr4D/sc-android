/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 * 
 * Distributable under LGPL license. See terms of license at gnu.org.
 */
package net.java.sip.communicator.android.service.gui;

import android.content.*;
import android.view.*;

/**
 * The <tt>UIService</tt> offers generic access to the graphical user
 * interface for all modules that would like to interact with the user.
 * <p>
 * Through the <tt>UIService</tt> all modules can add their own components in
 * different menus, toolbars, etc. within the ui. Each <tt>UIService</tt>
 * implementation should export its supported "plugable" containers - a set of
 * <tt>Container</tt>s corresponding to different "places" in the
 * application, where a module can add a component.
 * <p>
 * The <tt>UIService</tt> provides also methods that would allow to other
 * modules to control the visibility, size and position of the main application
 * window. Some of these methods are: setVisible, minimize, maximize, resize,
 * move, etc.
 * <p>
 * A way to show different types of simple windows is provided to allow other
 * modules to show different simple messages, like warning or error messages. In
 * order to show a simple warning message, a module should invoke the
 * getPopupDialog method and then one of the showXXX methods, which corresponds
 * best to the required dialog.
 * <p>
 * Certain components within the GUI, like "AddContact" window for example,
 * could be also shown from outside the ui. To make one of these component
 * exportable, the <tt>UIService</tt> implementation should attach to it an
 * <tt>WindowID</tt>. A window then could be shown, by invoking
 * <code>getExportedWindow(WindowID)</code> and then <code>show</code>. The
 * <tt>WindowID</tt> above should be obtained from
 * <code>getSupportedExportedWindows</code>.
 * 
 * @author Yana Stamcheva
 */
public interface UIService
{
    /**
     * Gets an object of type AccountRegistrationWizardContainer,but since this
     * interface should also be made available in the host application, part of
     * the Android APK, where AccountRegistrationWizardContainer is not and
     * cannot be made available, it returns an Object; any class that invokes it
     * should make a cast to AccountRegistrationWizardContainer
     * 
     * @return
     */
    public Object getAccountRegWizardContainer();

    /**
     * Gets android Context class needed when building views for SIP
     * Communicator UI.
     * 
     * @return
     */
    public Context getAndroidContext();

    /**
     * Method returning the root container of the application UI.
     * 
     * @return View object representing 
     */
    public View getRootView();

    /**
     * Initializes all frames and panels and shows the gui.
     */
    public void loadApplicationGui();

    /**
     * Gets the view corresponding to a new account registration wizzard.
     * 
     * @return View object
     */
    public View displayNewAccountRegistrationWizard();

    /**
     * Gets the view corresponding to a new account registration wizzard.
     * 
     * @return View object
     */
    public View displayAddContactWizard();
}
